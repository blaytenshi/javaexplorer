package com.jimmy.experimentals;

import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public class Lambda {

    public static void main(String... args) {

        List<String> names = Arrays.asList("peter", "anna", "mike", "xenia");

        Collections.sort(names, new Comparator<String>() {

            @Override
            public int compare(String a, String b) {
                return b.compareTo(a); // sorts them in reverse alphabetical order
            }

        });

        /* Shorter Version
        Collections.sort(names, (String a, String b) -> b.compareTo(a));
         */

        /* Even shorter still
        Collections.sort(names, (a, b) -> b.compareTo(a));
         */

        for(String name : names) {
            System.out.println(name);
        }

    }

}
