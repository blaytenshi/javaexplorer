package com.jimmy.experimentals;

public class ConverterMain {

    public static void main(String... args) {

        Converter <String, Integer> converter = (from) -> Integer.valueOf(from);
        Integer converted = converter.convert("123");
        System.out.println(converted);

    }

}
