package com.jimmy.experimentals;

public class Main {

        public static void main(String... args) {

        // because the sqrt() function is already defined as a default, we only need to override the calculate() function
        Formula formula = new Formula() {

            @Override
            public double calculate(int a) {
                return sqrt(a * 100); // calls the sqrt default method in Formula interface
            }

        };

        System.out.println(formula.calculate(100)); // square root of 10000 is 100
        System.out.println(formula.sqrt(16)); // calling formula's default sqrt function
    }

}
