package com.jimmy.experimentals;

@FunctionalInterface
public interface Converter<F, T> {

    T convert(F from);

}
