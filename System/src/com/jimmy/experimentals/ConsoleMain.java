package com.jimmy.experimentals;

import java.io.Console;
import java.util.Scanner;

public class ConsoleMain {

    public static void main(String[] args) {

        // Note this will not work inside an IDE as it attempts to get the actual console!
        // Will need to run this with javac ConsoleMain.java
        // Followed by java -cp D:/(path to start of src folder) com.jimmy.experimentals.ConsoleMain

        Console con = System.console();
        if (con == null) {
            System.err.println("Console Object not available.");
            System.exit(1);
        }

        String name = con.readLine("Enter your Name: ");
        con.printf("Hello %s%n", name);

        Scanner in = new Scanner(con.reader());

        con.printf("Enter an integer: ");
        int anInt = in.nextInt();

        con.printf("The integer entered is %d%n", anInt);
        con.printf("Enter a floating point number: ");
        double aDouble = in.nextDouble();
        con.printf("The floating number entered is %f%n", aDouble);
        in.close();
    }
}
