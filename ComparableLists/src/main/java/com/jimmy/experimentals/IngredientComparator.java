package com.jimmy.experimentals;

import java.util.Comparator;

/**
 * Created by jtang on 11/6/17.
 */
public class IngredientComparator implements Comparator<Ingredient> {
    public int compare(Ingredient ingredient1, Ingredient ingredient2) {

        int returnValue = ingredient1.getDisplayName().compareTo(ingredient2.getDisplayName());

        if (returnValue > 0) {
            return 1;
        } else if (returnValue == 0) {
            return 0;
        } else {
            return -1;
        }

    }
}
