package com.jimmy.experimentals;

/**
 * Created by jtang on 9/6/17.
 */
public class Block implements Comparable<Block> {

    private String name;

    private String colour;

    public Block() {

    }

    public Block(String colour) {
        this.colour = colour;
    }

    public Block(String name, String colour) {
        this.name = name;
        this.colour = colour;
    }

    public String getName() {
        return name;
    }

    public String getColour() {
        return colour;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Block)) return false;

        Block block = (Block) o;

        if (!getName().equals(block.getName())) return false;
        return getColour().equals(block.getColour());
    }

    @Override
    public int hashCode() {
        int result = getName().hashCode();
        result = 31 * result + getColour().hashCode();
        return result;
    }

    public int compareTo(Block block) {

        int returnValue = this.name.compareTo(block.getName());

        if (returnValue > 0) {
            return 1;
        } else if (returnValue == 0) {
            return 0;
        } else {
            return -1;
        }
    }
}
