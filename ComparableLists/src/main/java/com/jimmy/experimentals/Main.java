package com.jimmy.experimentals;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;

/**
 * Created by blaytenshi on 7/06/2017.
 */
public class Main {

    public static void main(String... args) {

        Block redBlock = new Block("greg", "red");
        Block blueBlock = new Block("john", "blue");
        Block greenBlock = new Block("andrew", "green");
        Block yellowBlock = new Block("harold", "yellow");

        // Sorting an array of Blocks
        Block[] blocksArray = new Block[4];

        blocksArray[0] = redBlock;
        blocksArray[1] = blueBlock;
        blocksArray[2] = greenBlock;
        blocksArray[3] = yellowBlock;
        System.out.println("Printing name of blocks in Array in insertion order: ");
        for (int i = 0; i < blocksArray.length; i++) {
            System.out.println(blocksArray[i].getName());
        }

        System.out.println("Printing name of blocks in Array in sorted order: ");
        Arrays.sort(blocksArray);
        for (int i = 0; i < blocksArray.length; i++) {
            System.out.println(blocksArray[i].getName());
        }

        // Sorting an ArrayList of Blocks
        ArrayList<Block> blockArrayList = new ArrayList<Block>();

        blockArrayList.add(redBlock);
        blockArrayList.add(blueBlock);
        blockArrayList.add(greenBlock);
        blockArrayList.add(yellowBlock);
        System.out.println("Printing name of blocks in ArrayList in insertion order: ");
        for (int i = 0; i < blockArrayList.size(); i++) {
            System.out.println(blockArrayList.get(i).getName());
        }

        System.out.println("Printing name of blocks in ArrayList in sorted order: ");
        Collections.sort(blockArrayList);
        for (int i = 0; i < blockArrayList.size(); i++) {
            System.out.println(blockArrayList.get(i).getName());
        }

    }

}
