package com.jimmy.experimentals;

/**
 * Created by blaytenshi on 7/06/2017.
 */

// Ingredient.values() returns an object with all

public enum Ingredient {

    ANCHOVIE("Anchovie", 1.0),
    PINEAPPLE("Pineapple", 1.0),
    CHICKEN("Chicken", 2.5),
    CHEESE("Cheese", 1.5);

    private final String displayName;
    private final double price;

    Ingredient(String displayName, double price) {
        this.displayName = displayName;
        this.price = price;
    }

    double getPrice() {
        return price;
    }

    String getDisplayName() {
        return displayName;
    }

}
