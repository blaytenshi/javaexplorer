package com.jimmy.experimentals;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;

/**
 * Created by blaytenshi on 7/06/2017.
 */
public class IngredientsListManager {

    ArrayList<Ingredient> kil;
    ArrayList<Ingredient> pil;

    public IngredientsListManager(
            ArrayList<Ingredient> kitchenIngredientsList,
            ArrayList<Ingredient> pizzaIngredientsList) {
        this.kil = kitchenIngredientsList;
        this.pil = pizzaIngredientsList;
    }

    public ArrayList<Ingredient> getKitchenIngredientsList() {
        return kil;
    }

    public ArrayList<Ingredient> getPizzaIngredientList() {
        return pil;
    }

    public boolean addToPizza(Ingredient ingredient) {

        if ((!pil.contains(ingredient)) && kil.contains(ingredient)) {
            // add ingredient if it is not already in the list
            if (pil.add(ingredient)) {
                // return true if successfully added
                return true;
            } else {
                // return false if unsuccessful
                return false;
            }
        } else {
            // return false if already in the list
            return false;
        }

    }

    public void sortKitchenIngredientsList() {
        Collections.sort(kil, new IngredientComparator());
    }

}
