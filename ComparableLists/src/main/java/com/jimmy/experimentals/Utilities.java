package com.jimmy.experimentals;

import java.util.ArrayList;

/**
 * Created by blaytenshi on 8/06/2017.
 */
public class Utilities {

    public Utilities() {

    }

    public static ArrayList<Ingredient> buildKitchenIngredientsListWithAllIngredients() {

        ArrayList<Ingredient> kitchenWithAllIngredients = new ArrayList<Ingredient>();

        for (Ingredient ingredient : Ingredient.values()) {
            kitchenWithAllIngredients.add(ingredient);
        }

        return kitchenWithAllIngredients;

    }

}
