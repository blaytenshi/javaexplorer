package com.jimmy.experimentals;

import org.junit.Test;

import java.util.ArrayList;

import static org.assertj.core.api.Assertions.*;

/**
 * Created by blaytenshi on 7/06/2017.
 */
public class IngredientsListManagerTest {

    IngredientsListManager ilm;
    ArrayList<Ingredient> pizzaIngredientsList;
    ArrayList<Ingredient> kitchenIngredientsList;

    @Test
    public void shouldReturnTrueIfItemIsAddedToPizzaList() throws Exception {

        kitchenIngredientsList = Utilities.buildKitchenIngredientsListWithAllIngredients();
        pizzaIngredientsList = new ArrayList<Ingredient>();
        ilm = new IngredientsListManager(kitchenIngredientsList, pizzaIngredientsList);

        assertThat(ilm.addToPizza(Ingredient.ANCHOVIE)).isTrue();
        assertThat(ilm.getPizzaIngredientList().contains(Ingredient.ANCHOVIE));

    }

    @Test
    public void shouldReturnFalseIfDuplicateItemIsAddedToPizzaList() throws Exception {

        kitchenIngredientsList = Utilities.buildKitchenIngredientsListWithAllIngredients();
        pizzaIngredientsList = new ArrayList<Ingredient>();
        ilm = new IngredientsListManager(kitchenIngredientsList, pizzaIngredientsList);
        ilm.addToPizza(Ingredient.ANCHOVIE); // should return true because original pizza list was empty

        assertThat(ilm.addToPizza(Ingredient.ANCHOVIE)).isFalse(); // checks to make sure it refused to add
        assertThat(ilm.getPizzaIngredientList().size()).isEqualTo(1); // checks to make sure nothing was added

    }

    @Test
    public void shouldReturnFalseIfItemDoesNotExistInKitchenIngredientsList() throws Exception {

        kitchenIngredientsList = Utilities.buildKitchenIngredientsListWithAllIngredients();
        kitchenIngredientsList.remove(Ingredient.ANCHOVIE);
        pizzaIngredientsList = new ArrayList<Ingredient>();
        ilm = new IngredientsListManager(kitchenIngredientsList, pizzaIngredientsList);

        // Makes sure there's no anchovies in Kitchen's Ingredient List
        assertThat(kitchenIngredientsList.contains(Ingredient.ANCHOVIE)).isFalse();
        assertThat(ilm.addToPizza(Ingredient.ANCHOVIE)).isFalse(); // should not be able to add it

        ArrayList<Ingredient> pizzaIngredientsList = ilm.getPizzaIngredientList();
        assertThat(pizzaIngredientsList.contains(Ingredient.ANCHOVIE)).isFalse(); // no anchovies added
        assertThat(pizzaIngredientsList.size()).isEqualTo(0); // list size should still be zero

        for (Ingredient ingredient : ilm.getKitchenIngredientsList()) {
            System.out.println(ingredient);
        }

    }

    @Test
    public void shouldSortKitchenIngredientsListAlphabetically() throws Exception {

        kitchenIngredientsList = Utilities.buildKitchenIngredientsListWithAllIngredients();
        pizzaIngredientsList = new ArrayList<Ingredient>();
        ilm = new IngredientsListManager(kitchenIngredientsList, pizzaIngredientsList);
        ilm.sortKitchenIngredientsList();
        ArrayList<Ingredient> retrievedKitchenIngredientsList = ilm.getKitchenIngredientsList();
        assertThat(retrievedKitchenIngredientsList.get(0)).isEqualTo(Ingredient.ANCHOVIE);
        assertThat(retrievedKitchenIngredientsList.get(1)).isEqualTo(Ingredient.CHEESE);
        assertThat(retrievedKitchenIngredientsList.get(2)).isEqualTo(Ingredient.CHICKEN);
        assertThat(retrievedKitchenIngredientsList.get(3)).isEqualTo(Ingredient.PINEAPPLE);

    }
}