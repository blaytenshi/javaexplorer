package com.jimmy.experimentals;

public class PassByReferenceValue {

    // When calling a method, are the parameters passed into that method passed by reference or pass by value?

    // Pass by Value (in the Stack Memory)
    // Java is always passed by value in the memory stack. What does this mean? Java has two types of memory, stack
    // memory and heap memory. When Java creates a variable that is a primitive, it assigns the value of that primitive
    // directly into the memory cell in the stack. When that variable is an object, it puts that object in heap memory
    // and stores the MEMORY ADDRESS of that object into the stack memory.

    // Pass by Reference (or POINTER to the value)
    // Other languages pass by reference meaning the method ends up manipulating the actual object itself.

    public static void main(String[] args) {
        ReferenceObject ro = new ReferenceObject("f");
        changeReference(ro);
        modifyReference(ro);

        int x = 1;
        int y = 2;
        swap(x, y);
    }

    public static void changeReference(ReferenceObject a /* = ro */) {
        // ro's value in the stack (which is its memory address for objects) is copied into a)
        ReferenceObject b = new ReferenceObject("b");
        a = b;
        // after this method completes execution, a now has no reference because a is just a copy of ro not actual ro
        // as such, the copy will fail and ro will remain as it is
    }

    public static void modifyReference(ReferenceObject c /* = ro */) {
        c.setName("c");
        // this will work because at the top, c's reference is pointed at the reference that ro is pointed to
    }
    
    private static void swap(int a /* = x */ , int b /* = y */) {
        // x and y's value in the stack (which is actual values 1 and 2) is copied into a and b respectively.

        // temp is initialised with 0 because primitive int is initialised with 0 if nothing is given
        int temp;

        // temp gets a copy of the value of a (which is given a copy of the value of x)
        temp = a;
        // a gets a copy of the value of b (which is given a copy of the value of y)
        a = b;
        // b gets a copy of the value of temp (which is given a copy of the value of a which is a given a copy of the value of x)
        b = temp;

        // they all actually swap! BUT because a and b only contain a COPY of the values of x and y, if you print x and y after the execution of this function, they still retain their original values.. gg idiot
    }

}
