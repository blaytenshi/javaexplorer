import '../styles/image_viewer.css'; //remember to add the file extension on for files that aren't js
import small from '../assets/small.jpg'; //injected into the bundle.js when we do npm run build

export default () => {
    const smallImage = document.createElement('img');
    smallImage.src = small;

    document.body.appendChild(smallImage); //we can use small here as it is an object inside the built bundle.js
};