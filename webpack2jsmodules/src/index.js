const button = document.createElement('button');
button.innerText = 'Click me';
button.onclick = () => {
    // System is a global variable inside JavaScript (ES2015). It'll fetch the specified file and send it back.
    // If the file being imported has by the System.import has import statements, it will fetch THOSE imports too.
    // Also remember that the System.import() is an async type call. Therefore this import() call returns a promise.
    // When webpack encounters System.import it will automatically inject code into the bundle.js to allow JSONP fetching of the seperate js files.
    System.import('./image_viewer').then(module => {
        module.default();
    })
};

document.body.appendChild(button);