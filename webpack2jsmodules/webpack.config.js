// even the config file can have require statments. This is handled by nodejs not by webpack. Therefore we can utilise any piece of nodejs technology we want inside the tooling pipeline
const path = require('path'); // importing node's path module
const ExtractTextPlugin = require('extract-text-webpack-plugin');

const config = {
    entry: './src/index.js', //the first file for webpack to examine and start the module building process
    output: {
        path: path.resolve(__dirname, 'build'), //reference to the directory that the output should be saved to. This has to be ABSOLUTE PATH reference. We're using resolve(__dirname) which allows us to resolve the correct path in different OS's. The build is the build folder.
        filename: 'bundle.js', //conventionally bundle.js but you can name it whatever you want
        publicPath: 'build/'
    },
    module: { //module/rule system in Webpack 2
        rules: [
            {
                use: 'babel-loader',
                test: /\.js$/
            },
            {
                loader: ExtractTextPlugin.extract({
                    loader: 'css-loader'
                }),//user and loader are exactly the same. However extract-text-plugin needs loader instead
                test: /\.css$/
            },
            {
                use: [
                    {
                        loader: 'url-loader',
                        options: { limit: 40000 } //if greater than 40kbytes save as a file, less than include as raw data inside the bundle.js
                    },
                    'image-webpack-loader'
                ],
                test: /\.(jpe?g|png|gif|svg)$/
            }
        ]
    },
    plugins: [
        new ExtractTextPlugin('style.css') //whatever was extracted by the plugin gets put into a style.css file
    ]
};

module.exports = config;