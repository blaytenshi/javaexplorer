package com.jimmy.experimentals;

import java.util.Arrays;
import java.util.Scanner;

public class GradesAverage {

    /*
        [x] Prompts user for the number of students
        [x] Reads it from keyboard
        [x] Saves it in an int variable called numStudents
        [x] Prompts user for the grades of each of the students and saves them in an int array called grades
        [x] Check grade between 0 and 100
        [x] Shows grade average in the end
        [x] BONUS - Array to string
     */

    public static void main(String[] args) {

        // Prepare variables for number of students and gradeTotal (Not average)
        int numStudents;
        double gradeTotal = 0;

        // Instantiate a scanner
        Scanner in = new Scanner(System.in);

        // Print instruction and wait reading in of number of students
        System.out.print("Please enter number of students: ");
        numStudents = in.nextInt();

        // Setup array with given number of students
        int[] grades = new int[numStudents];

        // Iterate through array and fill with student grades
        for(int i = 0; i < grades.length; i++) {

            // Prepare variable for grade
            int grade;
            // Ask for grade
            System.out.println("Grade of Student number " + (i+1));
            grade = in.nextInt();

            // Check the inputted grade. While it's out of value restrictions, print error message and try again
            while (grade < 0 || grade > 100) {
                System.out.println("Invalid grade, try again...");
                System.out.println("Grade of Student number " + (i+1));
                grade = in.nextInt();
            }

            // Once value restrictions are passed, store in grades array
            grades[i] = grade;

            // Calculate the total as we go
            gradeTotal = gradeTotal + grade;
        }

        // Print out average (grade total / number of students)
        System.out.println("The grade average is: " + gradeTotal/numStudents);

        // Print out the contents of the array to prove we got them
        // Arrays.toString is a java.util that is used to transform primitive arrays into strings
        System.out.println("The grades are: " + Arrays.toString(grades));

        // Close the scanner
        in.close();

    }
}
