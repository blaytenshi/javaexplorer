// Node Server
const express = require('express');
const path = require('path');
const app = express();

// server routes
app.get('/artist', (req, res) => res.send({ hi: 'there'}));

if (process.env.NODE_ENV !== 'production') {
    //the middleware to be used by node server
    const webpackMiddleware = require('webpack-dev-middleware');
    //the webpack library
    const webpack = require('webpack');
    //the webpack config file that we have at the root of this folder atm
    const webpackConfig = require('./webpack.config.js');
    app.use(webpackMiddleware(webpack(webpackConfig))); //webpack dev server running inside the node server now
} else {
    app.use(express.static('dist')); //allow anyone to get the files inside this directory
    app.get('*', (req, res) => {
        res.sendFile(path.join(__dirname, 'dist/index.html')); //if anyone makes a get request to any route on the server, send back the html file
    });
}

//ports need to be set. When deployed in production environment you'll need to config the port. Otherwise fallback on 3050 if running on dev environment.
app.listen(process.env.PORT || 3050, () => console.log('Listening'));