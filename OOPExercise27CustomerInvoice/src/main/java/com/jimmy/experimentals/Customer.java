package com.jimmy.experimentals;

public class Customer {

    private String name;
    private int id;
    private int discount;

    public Customer(int id, String name, int discount) {
        this.name = name;
        this.id = id;
        this.discount = discount;
    }

    public String getName() {
        return name;
    }

    public int getId() {
        return id;
    }

    public int getDiscount() {
        return discount;
    }

    public String toString() {
        return name + "(" + id + ")";
    }
}
