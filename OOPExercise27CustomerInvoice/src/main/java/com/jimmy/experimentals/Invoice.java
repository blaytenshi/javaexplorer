package com.jimmy.experimentals;

/**
 * Created by jtang on 21/1/17.
 */
public class Invoice {

    private int id;
    private Customer customer;
    private double invoiceAmount;

    public Invoice(int id, Customer customer, double invoiceAmount) {
        this.id = id;
        this.customer = customer;
        this.invoiceAmount = invoiceAmount;
    }

    public int getId() {
        return id;
    }

    public Customer getCustomer() {
        return customer;
    }

    public double getAmount() {
        return invoiceAmount;
    }

    public String getCustomerName() {
        return customer.getName();
    }

    public double getAmountAfterDiscount() {
        double amountToDiscount = invoiceAmount * (customer.getDiscount()/100.0);
        return invoiceAmount - amountToDiscount;
    }
}
