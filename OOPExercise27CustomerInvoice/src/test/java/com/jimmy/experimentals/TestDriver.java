package com.jimmy.experimentals;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

public class TestDriver {

    private Customer customer;
    private Invoice invoice;

    @Before
    public void before() {
        customer = new Customer(1, "John", 10);
        invoice = new Invoice(1, customer, 100.0);
    }

    @Test
    public void customerIsNotNull() {
        assertNotNull(customer);
    }

    @Test
    public void customerHasProperties() {
        assertEquals(1, customer.getId());
        assertEquals("John", customer.getName());
        assertEquals(10, customer.getDiscount());
    }

    @Test
    public void customerToStringReturnsCorrectFormat() {
        assertEquals("John(1)", customer.toString());
    }

    @Test
    public void invoiceIsNotNull() {
        assertNotNull(invoice);
    }

    @Test
    public void invoiceHasProperties() {
        assertEquals(1, invoice.getId());
        assertEquals(customer, invoice.getCustomer());
        assertEquals(100.0, invoice.getAmount(), 0);
        assertEquals("John", invoice.getCustomerName());
        assertEquals(90.0, invoice.getAmountAfterDiscount(), 0);
    }

}
