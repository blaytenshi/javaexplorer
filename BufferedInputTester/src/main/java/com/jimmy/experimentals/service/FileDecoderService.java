package com.jimmy.experimentals.service;

import org.springframework.stereotype.Component;

import java.io.BufferedInputStream;
import java.io.IOException;
import java.util.Arrays;

@Component
public class FileDecoderService {
    private BufferedInputStream bufferedInputStream;
    private byte image[] = new byte[0];
    private byte read[] = new byte[2048];

    public FileDecoderService(BufferedInputStream bufferedInputStream) {
        this.bufferedInputStream = bufferedInputStream;
    }

    public String getBoundary() throws IOException {
        char buf[] = new char[255];

        int len = 0;

        buf[len++] = (char) bufferedInputStream.read();

        buf[len++] = (char) bufferedInputStream.read();

        while(buf[len - 2] != '\r' && buf[len - 1] != '\n') {
            if (len == buf.length) {
                buf = Arrays.copyOf(buf, buf.length * 2);
            }
            buf[len++] = (char) bufferedInputStream.read();
        }

        return String.valueOf(buf, 0, len).trim();

    }

    public boolean isFFD8Present() throws IOException {
        byte readBuffer[] = new byte[255];

        do {
            // read first two bytes into byte array
            readBuffer[0] = (byte) bufferedInputStream.read();
            readBuffer[1] = (byte) bufferedInputStream.read();
            // if the two recently read bytes are matching, return
            if (readBuffer[0] == (byte) 0xFF && readBuffer[1] == (byte) 0xD8) {
                System.out.println("Start of jpg bytes found!");
                return true;
            }
        } while (bufferedInputStream.available() > 0);

        System.out.println("Start of jpg bytes not found...");
        return false;

    }


    public boolean isFFD9Present() throws IOException {
        byte readBuffer[] = new byte[255];

        do {
            // read first two bytes into byte array
            readBuffer[0] = (byte) bufferedInputStream.read();
            readBuffer[1] = (byte) bufferedInputStream.read();
            // if the two recently read bytes are matching, return
            if (readBuffer[0] == (byte) 0xFF && readBuffer[1] == (byte) 0xD9) {
                System.out.println("Start of jpg bytes found!");
                return true;
            }
        } while (bufferedInputStream.available() > 0);

        System.out.println("Start of jpg bytes not found...");
        return false;
    }

    private int getLocationOfFFD8(byte[] read) {
        for(int i = 0; i < read.length; i++) {
            if (read[i] == (byte) 0xFF && read[i+1] == (byte) 0xD8) {
                return i;
            }
        }
        // FFD8 not found
        return -1;
    }

    public byte[] extractFrame() throws IOException {

        // read into a buffer
        while (bufferedInputStream.available() > 0) {
            if (bufferedInputStream.read(read, 0, 2048) > 0) {
                // check the container of buffer, does it contain FFD8?
                int FFD8Location = getLocationOfFFD8(read);
                if (FFD8Location == -1) {
                    Arrays.copyOf(read, image.length + read.length);
                    System.arraycopy(read, 0, image, image.length + 1, read.length);
                } else if (FFD8Location >= 0) {
                    Arrays.copyOf(image, image.length + read.length - FFD8Location);
                    System.arraycopy(read, FFD8Location, image, image.length + 1, read.length);
                }
            }
        }

        return image;
    }

    public byte[] extract() throws IOException {
        byte[] buf = new byte[1024];
        int pos = 0, tmp;

        while (true) {

            // fill buf with data from stream, get amount of bytes that has been filled into buf
            if ((tmp = bufferedInputStream.read(buf, pos, buf.length - pos)) < 0)
            {
                System.out.println("End of image file.");
                System.out.println(Byte.toUnsignedInt(buf[pos + tmp - 1]));
                System.out.println(Integer.toHexString(Byte.toUnsignedInt(buf[pos + tmp])));
                return null;
            }


            // iterate through buf that contains data from stream
            for (int i = pos; i <= pos + tmp; i++) {
                if (i > 0 && buf[i - 1] == ((byte)0xFF) && buf[i] == ((byte)0xD9)) { // end of image found
                    return Arrays.copyOf(buf, i+1);
                }

//                if (i == pos + tmp)
//                {
//                    System.out.println();
//                }

            }

            pos += tmp;
            if (pos == buf.length) buf = Arrays.copyOf(buf, buf.length * 2);
        }

    }
}

