package com.jimmy.experimentals;

import com.jimmy.experimentals.service.FileDecoderService;
import com.jimmy.experimentals.service.HelloWorldService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SimpleApplication implements CommandLineRunner {

    @Autowired
    private HelloWorldService helloWorldService;

    @Autowired
    private FileDecoderService fileDecoderService;

    @Override
    public void run(String... args) throws Exception {
        System.out.println(this.helloWorldService.getHelloMessage());
        System.out.println(this.fileDecoderService.isFFD8Present());
		if (args.length > 0 && args[0].equals("exitcode")) {
            throw new ExitException();
        }
    }

    public static void main(String... args) throws Exception {
        SpringApplication.run(SimpleApplication.class, args);
    }
}
