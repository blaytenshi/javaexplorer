package com.jimmy.experimentals;

import org.springframework.boot.ExitCodeGenerator;

public class ExitException extends RuntimeException implements ExitCodeGenerator {

    // See here for why we have this exception (Ans: makes exit code testable)
    // https://sdqali.in/blog/2016/04/17/programmable-exit-codes-for-spring-command-line-applications/

    @Override
    public int getExitCode() {
        return 0;
    }

}
