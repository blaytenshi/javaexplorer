package com.jimmy.experimentals;

import java.io.BufferedInputStream;
import java.io.IOException;
import java.util.Arrays;

public class Experiment {

    public static void main(String... args) throws IOException {

        BufferedInputStream bufferedInputStream = new BufferedInputStream(Experiment.class.getClass().getClassLoader().getResourceAsStream("pathetic.jpg"));

        byte[] image;
        byte[] buf = new byte[10240];
        int pos = 0, tmp;

        while (bufferedInputStream.available() > 0) {
            // fill buf with data from stream, get amount of bytes that has been filled into buf
            tmp = bufferedInputStream.read(buf, pos, buf.length - pos);

            // iterate through buf that contains data from stream
            for (int i = pos; i < pos + tmp; i++) {
                if (buf[i] == 0xFF && buf[i+1] == 0xD9) { // end of image
                    if (tmp == buf.length) {
                        buf = Arrays.copyOf(buf, buf.length * 2);
                    }
                    image = buf;                          // set the buffer to the image
                }
                // set the read up to position (tmp) as the new pos
                pos = tmp;
            }
        }
    }
}
