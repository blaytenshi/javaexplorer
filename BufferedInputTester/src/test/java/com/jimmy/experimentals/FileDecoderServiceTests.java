package com.jimmy.experimentals;

import com.jimmy.experimentals.service.FileDecoderService;
import org.junit.Test;

import java.io.BufferedInputStream;

import static org.assertj.core.api.Assertions.assertThat;

public class FileDecoderServiceTests {

    @Test
    public void shouldReturnBoundaryValue() throws Exception {

        ClassLoader classLoader = getClass().getClassLoader();

        BufferedInputStream bufferedInputStream = new BufferedInputStream(classLoader.getResourceAsStream("test.txt"));
        FileDecoderService fileDecoderService = new FileDecoderService(bufferedInputStream);

        String boundary = fileDecoderService.getBoundary();

        assertThat(boundary).isEqualTo("--MOBOTIX_Fast_Serverpush");

        bufferedInputStream.close();
    }

    @Test
    public void shouldReturnTrueIfFFD8Found() throws Exception {
        ClassLoader classLoader = getClass().getClassLoader();

        BufferedInputStream bufferedInputStream = new BufferedInputStream(classLoader.getResourceAsStream("block.jpg"));
        FileDecoderService fileDecoderService = new FileDecoderService(bufferedInputStream);

        boolean found = fileDecoderService.isFFD8Present();

        assertThat(found).isTrue();

        bufferedInputStream.close();
    }

    @Test
    public void shouldTrueIfFFD9Found() throws Exception {
        ClassLoader classLoader = getClass().getClassLoader();

        BufferedInputStream bufferedInputStream = new BufferedInputStream(classLoader.getResourceAsStream("block.jpg"));
        FileDecoderService fileDecoderService = new FileDecoderService(bufferedInputStream);

        boolean found = fileDecoderService.isFFD9Present();

        assertThat(found).isTrue();

        bufferedInputStream.close();

    }

    @Test
    public void should() throws Exception {
        ClassLoader classLoader = getClass().getClassLoader();

        BufferedInputStream bufferedInputStream = new BufferedInputStream(classLoader.getResourceAsStream("pathetic.jpg"));
        FileDecoderService fileDecoderService = new FileDecoderService(bufferedInputStream);

        byte found[] = fileDecoderService.extract();

        System.out.println(found.length);

        bufferedInputStream.close();

    }
}
