package com.jimmy.experimentals.webpackspringgradle.controllers;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class AnotherController {

    @RequestMapping("/url")
    public String getUrls() {
        return "Just change the code then rebuild. It will auto restart for you.";

    }
}
