var path = require('path');

module.exports = {
    entry: './src/main/js/index.js',
    output: {
        path: path.resolve(__dirname, 'src', 'main', 'resources', 'static', 'js'), // path to build to when we do 'webpack -p'
        publicPath: '/',
        filename: 'bundle.js'
    },
    module: {
        loaders: [{
            exclude: /node_modules/,
            loader: 'babel',
            query: {
                presets: ['react', 'es2015', 'stage-1']
            }
        }]
    },
    resolve: {
        extensions: ['', '.js', '.jsx']
    },
    devServer: {
        port: 8070,
        historyApiFallback: true,
        contentBase: './src/main/resources/static/'
    }
};