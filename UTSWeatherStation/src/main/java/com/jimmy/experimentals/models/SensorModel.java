package com.jimmy.experimentals.models;

import java.net.URL;

public class SensorModel {

    private Integer id;
    private String sensor;
    private String sensorDisplayName;
    private Boolean status;
    private Integer sensorCalibrations;
    private Integer hardwareId;
    private Integer sensorTypesId;
    private Integer locationsId;

    public SensorModel() {

    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getSensor() {
        return sensor;
    }

    public void setSensor(String sensor) {
        this.sensor = sensor;
    }

    public String getSensorDisplayName() {
        return sensorDisplayName;
    }

    public void setSensorDisplayName(String sensorDisplayName) {
        this.sensorDisplayName = sensorDisplayName;
    }

    public Boolean getStatus() {
        return status;
    }

    public void setStatus(Boolean status) {
        this.status = status;
    }

    public Integer getSensorCalibrations() {
        return sensorCalibrations;
    }

    public void setSensorCalibrations(Integer sensorCalibrations) {
        this.sensorCalibrations = sensorCalibrations;
    }

    public Integer getHardwareId() {
        return hardwareId;
    }

    public void setHardwareId(Integer hardwareId) {
        this.hardwareId = hardwareId;
    }

    public Integer getSensorTypesId() {
        return sensorTypesId;
    }

    public void setSensorTypesId(Integer sensorTypesId) {
        this.sensorTypesId = sensorTypesId;
    }

    public Integer getLocationsId() {
        return locationsId;
    }

    public void setLocationsId(Integer locationsId) {
        this.locationsId = locationsId;
    }
}
