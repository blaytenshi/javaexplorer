package com.jimmy.experimentals.models;

import javax.persistence.*;

public class PeopleCount {

    private Long id;
    private String serial;
    private String name;
    private String timestamp;
    private int in;
    private int out;

    public PeopleCount() {

    }

    public String getSerial() {
        return serial;
    }

    public void setSerial(String serial) {
        this.serial = serial;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(String timestamp) {
        this.timestamp = timestamp;
    }

    public int getIn() {
        return in;
    }

    public void setIn(int in) {
        this.in = in;
    }

    public int getOut() {
        return out;
    }

    public void setOut(int out) {
        this.out = out;
    }

    @Override
    public String toString() {
        return "PeopleCount{" +
                "serial='" + serial + '\'' +
                ", name='" + name + '\'' +
                ", timestamp=" + timestamp +
                ", in=" + in +
                ", out=" + out +
                '}';
    }
}
