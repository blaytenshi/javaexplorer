package com.jimmy.experimentals.models;

import java.io.Serializable;
import java.util.List;

public class DataModel implements Serializable {

    private final static long serialVersionUID = 1L;

    private Integer count;
    private String sensor;
    private Integer status;
    private List<SampleModel> data;

    public DataModel() {

    }

    public DataModel(Integer count, String sensor, Integer status, List<SampleModel> data) {
        this.count = count;
        this.sensor = sensor;
        this.status = status;
        this.data = data;
    }

    public Integer getCount() {
        return count;
    }

    public void setCount(Integer count) {
        this.count = count;
    }

    public String getSensor() {
        return sensor;
    }

    public void setSensor(String sensor) {
        this.sensor = sensor;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public List<SampleModel> getData() {
        return data;
    }

    public void setData(List<SampleModel> data) {
        this.data = data;
    }
}
