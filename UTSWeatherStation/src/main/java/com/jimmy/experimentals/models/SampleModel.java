package com.jimmy.experimentals.models;

import com.jimmy.experimentals.entities.SampleEntity;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;

import java.io.Serializable;
import java.sql.Timestamp;

public class SampleModel implements Serializable {

    private final static long serialVersionUID = 1L;

    private Timestamp timestamp;
    private String timestampTxt;
    private Double value;

    public SampleModel() {

    }

    public SampleModel(SampleEntity sampleEntity) {
        this.timestamp = sampleEntity.getTimestamp();
        this.timestampTxt = sampleEntity.getTimestamp().toLocalDateTime().toString();
        this.value = sampleEntity.getValue();
    }

    public Timestamp getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(Timestamp timestamp) {
        this.timestamp = timestamp;
    }

    public String getTimestampTxt() {
        return timestampTxt;
    }

    public void setTimestampTxt(String timestampTxt) {
        this.timestampTxt = timestampTxt;
    }

    public Double getValue() {
        return value;
    }

    public void setValue(Double value) {
        this.value = value;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;

        if (o == null || getClass() != o.getClass()) return false;

        SampleModel that = (SampleModel) o;

        return new EqualsBuilder()
                .append(timestamp, that.timestamp)
                .append(timestampTxt, that.timestampTxt)
                .append(value, that.value)
                .isEquals();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder(17, 37)
                .append(timestamp)
                .append(timestampTxt)
                .append(value)
                .toHashCode();
    }
}
