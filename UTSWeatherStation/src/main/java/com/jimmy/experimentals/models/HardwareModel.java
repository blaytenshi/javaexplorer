package com.jimmy.experimentals.models;

import com.jimmy.experimentals.entities.HardwareEntity;

import java.io.Serializable;

public class HardwareModel implements Serializable {

    private final static long serialVersionUID = 1L;

    private String hardware;
    private String hardwareDisplayName;
    private String serialNumber;
    private String deviceUrl;

    public HardwareModel() {

    }

    public HardwareModel(String hardware, String hardwareDisplayName, String serialNumber, String deviceUrl) {
        this.hardware = hardware;
        this.hardwareDisplayName = hardwareDisplayName;
        this.serialNumber = serialNumber;
        this.deviceUrl = deviceUrl;
    }

    public static HardwareModel getModelFromEntity(HardwareEntity hardwareEntity) {
        return new HardwareModel(
            hardwareEntity.getHardware(), hardwareEntity.getHardwareDisplayName(), hardwareEntity.getSerialNumber(), hardwareEntity.getHardwareUrl()
        );
    }

    public String getHardware() {
        return hardware;
    }

    public void setHardware(String hardware) {
        this.hardware = hardware;
    }

    public String getHardwareDisplayName() {
        return hardwareDisplayName;
    }

    public void setHardwareDisplayName(String hardwareDisplayName) {
        this.hardwareDisplayName = hardwareDisplayName;
    }

    public String getSerialNumber() {
        return serialNumber;
    }

    public void setSerialNumber(String serialNumber) {
        this.serialNumber = serialNumber;
    }

    public String getDeviceUrl() {
        return deviceUrl;
    }

    public void setDeviceUrl(String deviceUrl) {
        this.deviceUrl = deviceUrl;
    }
}
