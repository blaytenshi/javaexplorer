package com.jimmy.experimentals.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.NOT_FOUND)
public class ResourceNotFoundException extends RuntimeException {

    private final static String RESOURCE_NOT_FOUND_FORMAT_STRING = "%s does not exist";

    public ResourceNotFoundException(Class thrownClass) {
        super(String.format(RESOURCE_NOT_FOUND_FORMAT_STRING, thrownClass.getSimpleName()));
    }

}
