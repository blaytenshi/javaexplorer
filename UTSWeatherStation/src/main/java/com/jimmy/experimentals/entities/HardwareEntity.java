package com.jimmy.experimentals.entities;

import javax.persistence.*;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name="hardware")
public class HardwareEntity implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    private String hardware;
    private String hardwareDisplayName;
    private String serialNumber;
    private String deviceUrl;

    @OneToMany(mappedBy = "hardware")
    private List<SensorEntity> sensors = new ArrayList<>();

    public HardwareEntity() {

    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getHardware() {
        return hardware;
    }

    public void setHardware(String hardware) {
        this.hardware = hardware;
    }

    public String getHardwareDisplayName() {
        return hardwareDisplayName;
    }

    public void setHardwareDisplayName(String hardwareDisplayName) {
        this.hardwareDisplayName = hardwareDisplayName;
    }

    public String getSerialNumber() {
        return serialNumber;
    }

    public void setSerialNumber(String serialNumber) {
        this.serialNumber = serialNumber;
    }

    public String getHardwareUrl() {
        return deviceUrl;
    }

    public void setHardwareUrl(String hardwareUrl) {
        this.deviceUrl = deviceUrl;
    }
}
