package com.jimmy.experimentals.entities;

import javax.persistence.*;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = "measurement_units")
public class MeasurementUnitsEntity implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    private String unit;

    @OneToMany(mappedBy = "measurementUnit")
    private List<SensorTypeEntity> sensorTypes = new ArrayList<>();

    public MeasurementUnitsEntity() {

    }

}
