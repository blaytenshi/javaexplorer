package com.jimmy.experimentals.entities;

import javax.persistence.*;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = "locations")
public class LocationEntity implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    private Double locationX;
    private Double locationY;
    private Double locationZ;

    @OneToMany(mappedBy = "location")
    private List<SensorEntity> sensors = new ArrayList<>();

    public LocationEntity() {

    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Double getLocationX() {
        return locationX;
    }

    public void setLocationX(Double locationX) {
        this.locationX = locationX;
    }

    public Double getLocationY() {
        return locationY;
    }

    public void setLocationY(Double locationY) {
        this.locationY = locationY;
    }

    public Double getLocationZ() {
        return locationZ;
    }

    public void setLocationZ(Double locationZ) {
        this.locationZ = locationZ;
    }
}
