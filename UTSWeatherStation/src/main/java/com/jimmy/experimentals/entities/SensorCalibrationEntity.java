package com.jimmy.experimentals.entities;

import javax.persistence.*;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = "sensor_calibrations")
public class SensorCalibrationEntity implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    private Double calibrationValue;

    @OneToMany(mappedBy = "sensorCalibration")
    private List<SensorEntity> sensors = new ArrayList<>();
}
