package com.jimmy.experimentals.entities;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.io.Serializable;

@Entity
@Table(name = "sensor_types")
public class SensorTypeEntity implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    private String sensorType;

    private String sensorTypeDisplayName;

    @ManyToOne
    @JoinColumn(name = "measurement_units_id")
    @NotNull
    private MeasurementUnitsEntity measurementUnit;

    public SensorTypeEntity() {

    }
}
