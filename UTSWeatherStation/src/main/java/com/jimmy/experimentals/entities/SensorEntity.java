package com.jimmy.experimentals.entities;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = "sensors")
public class SensorEntity implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    private String sensor;

    private String sensorDisplayName;

    private Boolean status;

    @ManyToOne
    @JoinColumn(name = "sensor_calibrations_id")
    @NotNull
    private SensorCalibrationEntity sensorCalibration;

    @ManyToOne
    @JoinColumn(name = "hardware_id")
    @NotNull
    private HardwareEntity hardware;

    @ManyToOne
    @JoinColumn(name = "sensor_types_id")
    @NotNull
    private SensorTypeEntity sensorType;

    @ManyToOne
    @JoinColumn(name = "locations_id")
    @NotNull
    private LocationEntity location;

    @OneToMany(mappedBy = "sensor")
    private List<SampleEntity> samples = new ArrayList<>();

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getSensor() {
        return sensor;
    }

    public void setSensor(String sensor) {
        this.sensor = sensor;
    }

    public String getSensorDisplayName() {
        return sensorDisplayName;
    }

    public void setSensorDisplayName(String sensorDisplayName) {
        this.sensorDisplayName = sensorDisplayName;
    }

    public Boolean getStatus() {
        return status;
    }

    public void setStatus(Boolean status) {
        this.status = status;
    }

    public SensorCalibrationEntity getSensorCalibration() {
        return sensorCalibration;
    }

    public void setSensorCalibration(SensorCalibrationEntity sensorCalibration) {
        this.sensorCalibration = sensorCalibration;
    }

    public HardwareEntity getHardware() {
        return hardware;
    }

    public void setHardware(HardwareEntity hardware) {
        this.hardware = hardware;
    }

    public SensorTypeEntity getSensorType() {
        return sensorType;
    }

    public void setSensorType(SensorTypeEntity sensorType) {
        this.sensorType = sensorType;
    }

    public LocationEntity getLocation() {
        return location;
    }

    public void setLocation(LocationEntity location) {
        this.location = location;
    }

    public List<SampleEntity> getSamples() {
        return samples;
    }

    public void setSamples(List<SampleEntity> samples) {
        this.samples = samples;
    }
}
