package com.jimmy.experimentals.entities;

import sun.management.Sensor;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.sql.Timestamp;
import java.time.LocalDateTime;

@Entity
@Table(name="samples")
public class SampleEntity implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy =  GenerationType.AUTO)
    private Long id;
    private Double value;
    private Timestamp timestamp;

    @ManyToOne
    @JoinColumn(name = "sensors_id")
    @NotNull
    private SensorEntity sensor;

    public SampleEntity() {

    }

    public SampleEntity(Double value, Timestamp timestamp, SensorEntity sensor) {
        this.value = value;
        this.timestamp = timestamp;
        this.sensor = sensor;
    }

    public Long getId() {
        return id;
    }

    public Double getValue() {
        return value;
    }

    public void setValue(Double value) {
        this.value = value;
    }

    public Timestamp getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(Timestamp timestamp) {
        this.timestamp = timestamp;
    }

    public SensorEntity getSensor() {
        return sensor;
    }

    public void setSensor(SensorEntity sensorEntity) {
        this.sensor = sensorEntity;
    }
}
