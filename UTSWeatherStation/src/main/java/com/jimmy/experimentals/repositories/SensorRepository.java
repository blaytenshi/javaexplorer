package com.jimmy.experimentals.repositories;

import com.jimmy.experimentals.entities.SensorEntity;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface SensorRepository extends CrudRepository<SensorEntity, Long> {
    SensorEntity findBySensor(String sensorName);
}
