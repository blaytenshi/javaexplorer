package com.jimmy.experimentals.repositories;

import com.jimmy.experimentals.entities.SampleEntity;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface SampleRepository extends CrudRepository<SampleEntity, Long> {

    SampleEntity findOne(Long id);

    SampleEntity findFirstByIdOrderByTimestampDesc(Long peopleCounterId);

}
