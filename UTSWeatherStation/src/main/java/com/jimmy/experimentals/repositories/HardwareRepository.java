package com.jimmy.experimentals.repositories;

import com.jimmy.experimentals.entities.HardwareEntity;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface HardwareRepository extends CrudRepository<HardwareEntity, Long> {
    HardwareEntity findBySerialNumber(String serialNumber);

    HardwareEntity findByHardware(String hardwareName);
}
