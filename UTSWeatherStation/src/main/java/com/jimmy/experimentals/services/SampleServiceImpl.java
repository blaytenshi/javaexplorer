package com.jimmy.experimentals.services;

import com.jimmy.experimentals.components.DataAcquisitionScheduler;
import com.jimmy.experimentals.entities.SampleEntity;
import com.jimmy.experimentals.entities.SensorEntity;
import com.jimmy.experimentals.models.SampleModel;
import com.jimmy.experimentals.repositories.HardwareRepository;
import com.jimmy.experimentals.repositories.SampleRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class SampleServiceImpl implements SampleService {

    private final SampleRepository sampleRepository;
    private final HardwareRepository hardwareRepository;
    private static final Logger log = LoggerFactory.getLogger(DataAcquisitionScheduler.class);

    @Autowired
    public SampleServiceImpl(SampleRepository sampleRepository, HardwareRepository hardwareRepository) {
        this.sampleRepository = sampleRepository;
        this.hardwareRepository = hardwareRepository;
    }

    @Override
    public SampleModel getLatestPeopleCounterSample(SensorEntity peopleCounter) {

        log.info("Sensor Entity Id: " + peopleCounter.getId());

        SampleEntity sampleEntity = sampleRepository.findFirstByIdOrderByTimestampDesc(peopleCounter.getId());

        log.info("Sample Entity Timestamp: " + sampleEntity.getTimestamp());

        SampleModel sampleModel = new SampleModel(sampleEntity);

        log.info("Timestamp " + sampleModel.getTimestamp());
        log.info("Timestamp Text" + sampleModel.getTimestampTxt());
        log.info("Value " + sampleModel.getValue());

        return sampleModel;
    }

    @Override
    public void saveSample(SampleEntity sampleEntity) {
        sampleRepository.save(sampleEntity);
    }


}
