package com.jimmy.experimentals.services;

import com.jimmy.experimentals.models.HardwareModel;

import java.util.List;

public interface HardwareService {
    List<HardwareModel> getAllHardware();

    HardwareModel getHardwareByHardwareName(String peopleCounterName);
}
