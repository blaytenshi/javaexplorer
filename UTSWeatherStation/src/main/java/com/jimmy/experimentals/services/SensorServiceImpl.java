package com.jimmy.experimentals.services;

import com.jimmy.experimentals.entities.HardwareEntity;
import com.jimmy.experimentals.entities.SensorEntity;
import com.jimmy.experimentals.repositories.HardwareRepository;
import com.jimmy.experimentals.repositories.SensorRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class SensorServiceImpl implements SensorService {

    private SensorRepository sensorRepository;
    private HardwareRepository hardwareRepository;

    @Autowired
    public SensorServiceImpl(SensorRepository sensorRepository) {
        this.sensorRepository = sensorRepository;
        this.hardwareRepository = hardwareRepository;
    }

    @Override
    public SensorEntity getSensorBySensorName(String sensorName) {
        return null;
    }

    @Override
    public SensorEntity getSensorBySensor(String sensorName) {
        SensorEntity sensorEntity = sensorRepository.findBySensor(sensorName);

        System.out.println("Sensor: " + sensorEntity.getId());

        return sensorEntity;
    }

}
