package com.jimmy.experimentals.services;

import com.jimmy.experimentals.entities.SensorEntity;
import com.jimmy.experimentals.models.DataModel;
import com.jimmy.experimentals.models.SampleModel;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class DataModelServiceImpl implements DataModelService {

    private SampleService sampleService;
    private SensorService sensorService;

    @Autowired
    public DataModelServiceImpl(SensorService sensorService, SampleService sampleService) {
        this.sampleService = sampleService;
        this.sensorService = sensorService;
    }

    @Override
    public DataModel getLatestPeopleCounter(String peopleCounterName) {

        SensorEntity sensorEntity = sensorService.getSensorBySensor(peopleCounterName);

        SampleModel sampleModel = sampleService.getLatestPeopleCounterSample(sensorEntity);

        List<SampleModel> sampleModelList = new ArrayList<>();
        sampleModelList.add(sampleModel);

        DataModel dataModel = new DataModel();

        dataModel.setCount(sampleModelList.size());
        dataModel.setSensor(peopleCounterName);
        dataModel.setStatus(200);
        dataModel.setData(sampleModelList);

        return dataModel;
    }
}
