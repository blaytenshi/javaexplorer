package com.jimmy.experimentals.services;

import com.jimmy.experimentals.entities.HardwareEntity;
import com.jimmy.experimentals.exceptions.ResourceNotFoundException;
import com.jimmy.experimentals.models.HardwareModel;
import com.jimmy.experimentals.repositories.HardwareRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;

@Service
public class HardwareServiceImpl implements HardwareService {

    private HardwareRepository hardwareRepository;

    @Autowired
    public HardwareServiceImpl(HardwareRepository hardwareRepository) {
        this.hardwareRepository = hardwareRepository;
    }

    @Override
    @Transactional
    public List<HardwareModel> getAllHardware() {
        List<HardwareEntity> hardware = (List<HardwareEntity>) hardwareRepository.findAll();

        // TODO: convert the hardwareEntity to hardwareModel List to pass into processing by scheduler
        return null;
    }

    @Override
    @Transactional
    public HardwareModel getHardwareByHardwareName(String hardwareName) {
        HardwareEntity hardwareEntity = hardwareRepository.findByHardware(hardwareName);

        if (hardwareEntity == null) {
            throw new ResourceNotFoundException(HardwareModel.class);
        }

        return HardwareModel.getModelFromEntity(hardwareEntity);
    }

}
