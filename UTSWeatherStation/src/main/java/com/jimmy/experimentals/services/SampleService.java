package com.jimmy.experimentals.services;

import com.jimmy.experimentals.entities.SampleEntity;
import com.jimmy.experimentals.entities.SensorEntity;
import com.jimmy.experimentals.models.SampleModel;

public interface SampleService {

    SampleModel getLatestPeopleCounterSample(SensorEntity people);

    void saveSample(SampleEntity sampleEntity);

}
