package com.jimmy.experimentals.services;


import com.jimmy.experimentals.entities.SensorEntity;

public interface SensorService {

    SensorEntity getSensorBySensor(String peopleCounterName);

}
