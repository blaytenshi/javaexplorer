package com.jimmy.experimentals.services;

import com.jimmy.experimentals.models.DataModel;

public interface DataModelService {

    DataModel getLatestPeopleCounter(String peopleCounterName);

}
