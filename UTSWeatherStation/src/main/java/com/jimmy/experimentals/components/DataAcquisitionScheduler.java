package com.jimmy.experimentals.components;

import com.jimmy.experimentals.services.SampleService;
import com.jimmy.experimentals.services.SensorService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.task.TaskExecutor;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

@Component
public class DataAcquisitionScheduler {

    private final TaskExecutor taskExecutor;
    private final SensorService sensorService;
    private final SampleService sampleService;

    @Autowired
    public DataAcquisitionScheduler(TaskExecutor taskExecutor, SensorService sensorService, SampleService sampleService) {
        this.taskExecutor = taskExecutor;
        this.sensorService = sensorService;
        this.sampleService = sampleService;
    }

    private static final Logger log = LoggerFactory.getLogger(DataAcquisitionScheduler.class);

    // second, minute, hour, day, day of month, month, day of week
    // run every 5th minute of every hour
    @Scheduled(cron = "0 */5 */1 * * *")
    public void acquireCounterData() {

        // Spin up threads in here!
        String[] urlList = {
                "http://hummingbird.feit.uts.edu.au:8080/peopleCounterApi/live/PC0214-BroadwayEastEntrance",
                "http://hummingbird.feit.uts.edu.au:8080/peopleCounterApi/live/PC0216-JonesStEntrance"
        };

        for (int i = 0; i < urlList.length; i++) {
            taskExecutor.execute(new FetchData(urlList[i], sensorService, sampleService));
            log.info("Executed Thread " + i);
        }

    }

}
