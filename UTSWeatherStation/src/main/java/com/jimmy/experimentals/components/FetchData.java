package com.jimmy.experimentals.components;

import com.jimmy.experimentals.Utilities.PeopleCounterUtilities;
import com.jimmy.experimentals.entities.SampleEntity;
import com.jimmy.experimentals.entities.SensorEntity;
import com.jimmy.experimentals.models.PeopleCount;
import com.jimmy.experimentals.services.SampleService;
import com.jimmy.experimentals.services.SensorService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.client.RestTemplate;

public class FetchData implements Runnable {

    private String url;
    private SensorService sensorService;
    private SampleService sampleService;

    private static final Logger log = LoggerFactory.getLogger(FetchData.class);

    public FetchData(String url, SensorService sensorService, SampleService sampleService) {
        this.url = url;
        this.sensorService = sensorService;
        this.sampleService = sampleService;
    }

    @Override
    public void run() {
        RestTemplate restTemplate = new RestTemplate();
        PeopleCount peopleCount = restTemplate.getForObject(url, PeopleCount.class);
        log.info(peopleCount.toString());

        SensorEntity sensorEntityIn = sensorService.getSensorBySensor(PeopleCounterUtilities.getSensorNameIn(peopleCount.getName()));
        SensorEntity sensorEntityOut = sensorService.getSensorBySensor(PeopleCounterUtilities.getSensorNameOut(peopleCount.getName()));

        SampleEntity sampleEntityIn = new SampleEntity((double) peopleCount.getIn(), PeopleCounterUtilities.convertPeopleCounterTime(peopleCount.getTimestamp()), sensorEntityIn);
        SampleEntity sampleEntityOut = new SampleEntity((double) peopleCount.getOut(), PeopleCounterUtilities.convertPeopleCounterTime(peopleCount.getTimestamp()), sensorEntityOut);

        sampleService.saveSample(sampleEntityIn);
        sampleService.saveSample(sampleEntityOut);
    }
}
