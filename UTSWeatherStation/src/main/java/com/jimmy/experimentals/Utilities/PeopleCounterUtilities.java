package com.jimmy.experimentals.Utilities;

import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.time.format.DateTimeFormatter;

public class PeopleCounterUtilities {

    public PeopleCounterUtilities() {

    }

    public static String getSensorNameIn(String name) {

        String sensorInName = name.replace(".", "").concat("_IN").toUpperCase();

        return sensorInName;
    }

    public static String getSensorNameOut(String name) {

        String sensorOutName = name.replace(".", "").concat("_OUT").toUpperCase();

        return sensorOutName;
    }

    public static Timestamp convertPeopleCounterTime(String stringTime) {
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyyMMddHHmmss");
        LocalDateTime localDateTime = LocalDateTime.parse(stringTime, formatter);

        return new Timestamp(localDateTime.toInstant(ZoneOffset.UTC).toEpochMilli());
    }

}
