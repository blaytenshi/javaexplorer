package com.jimmy.experimentals.controllers;

import com.jimmy.experimentals.models.DataModel;
import com.jimmy.experimentals.services.DataModelService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import static org.springframework.web.bind.annotation.RequestMethod.GET;

@RestController
@EnableAutoConfiguration
@RequestMapping("/peopleCounter/live")
public class PeopleCounterController {

    /*
    {
        sensor: "(sensor name)"
        status: (status code)
        dataCount: (number of data objects in data array)
        data: [
                {
                    timestamp: (utc timestamp from database)
                    timestampText: "(converted time from UTC time for debugging)"
                    reading: (value of reading from sample)
                 },
                 {...}
                ]
    }
    */

    private DataModelService dataModelService;

    @Autowired
    public PeopleCounterController(DataModelService dataModelService) {
        this.dataModelService = dataModelService;
    }

    @RequestMapping(method = GET, value = "/PC1201-Test")
    public String getTest() {
        return "{\"serial\":\"888888888888\",\"name\":\"PC12.01\",\"timestamp\":20170327123257,\"in\":502,\"out\":490}";
    }

    /*
    @RequestMapping(method = GET, value = "/{peopleCounterName}")
    public HardwareModel getPeopleCounter(
            @PathVariable("peopleCounterName") String peopleCounterName) {

        HardwareModel hardwareModel = hardwareService.getHardwareByHardwareName(peopleCounterName);

        return hardwareModel;

    }
    */

    @RequestMapping(method = GET, value = "/{peopleCounterName}")
    public DataModel getPeopleCount(
            @PathVariable("peopleCounterName") String peopleCounterName) {

        return dataModelService.getLatestPeopleCounter(peopleCounterName);

    }

}
