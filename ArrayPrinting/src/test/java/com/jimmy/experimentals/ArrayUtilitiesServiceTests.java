package com.jimmy.experimentals;

import org.junit.Before;
import org.junit.Test;

import static org.assertj.core.api.Assertions.assertThat;

public class ArrayUtilitiesServiceTests {

    private ArrayUtilitiesService arrayUtilitiesService;
    private int[] arrayOfNumbers1;
    private int[] arrayOfNumbers2;
    private int[] arrayOfNumbers3;

    @Before
    public void setUp() throws Exception {

        arrayUtilitiesService = new ArrayUtilitiesServiceImpl();
        arrayOfNumbers1 = new int[] {0, 2, 4, 1, 5};
        arrayOfNumbers2 = new int[] {0, 2, 4, 1, 5, 6};
        arrayOfNumbers3 = new int[] {0, 1, 2, 4, 5};

    }

    @Test
    public void shouldReturnStringOfArrayValues() throws Exception {

        String stringArray = arrayUtilitiesService.arrayToString(arrayOfNumbers1);

        assertThat(stringArray).isEqualTo("{0, 2, 4, 1, 5}");

    }

    @Test
    public void shouldReturnTrueIfIntValueExistsInArray() throws Exception {

        boolean found = arrayUtilitiesService.contains(1, arrayOfNumbers1);

        assertThat(found).isTrue();

    }

    @Test
    public void shouldReturnFalseIfIntValueDoesNotExistInArray() throws Exception {

        boolean found = arrayUtilitiesService.contains(8, arrayOfNumbers1);

        assertThat(found).isFalse();

    }

    @Test
    public void shouldReturnPositionOfFoundValue() throws Exception {

        int placeInArray = arrayUtilitiesService.search(1, arrayOfNumbers1);

        assertThat(placeInArray).isEqualTo(3);

    }

    @Test
    public void shouldReturnNegativeOneIfSearchNotFound() throws Exception {

        int cannotFindValue = arrayUtilitiesService.search(8, arrayOfNumbers1);

        assertThat(cannotFindValue).isEqualTo(-1);

    }

    @Test
    public void shouldReturnTrueIfArraysAreEqual() throws Exception {

        boolean arraysAreEqual = arrayUtilitiesService.equals(arrayOfNumbers1, arrayOfNumbers1);

        assertThat(arraysAreEqual).isTrue();

    }

    @Test
    public void shouldReturnFalseIfArraysAreNotEqualInLength() throws Exception {

        boolean arraysAreNotEqual = arrayUtilitiesService.equals(arrayOfNumbers1, arrayOfNumbers2);

        assertThat(arraysAreNotEqual).isFalse();

    }

    @Test
    public void shouldReturnFalseIfArraysAreSameLengthButDifferentContent() throws Exception {

        boolean arraysAreNotEqual = arrayUtilitiesService.equals(arrayOfNumbers1, arrayOfNumbers3);

        assertThat(arraysAreNotEqual).isFalse();

    }

}
