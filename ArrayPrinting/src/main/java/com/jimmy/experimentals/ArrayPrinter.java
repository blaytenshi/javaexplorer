package com.jimmy.experimentals;

import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ArrayPrinter implements CommandLineRunner {

    public static void main(String[] args) {
        System.exit(0);
    }

    @Override
    public void run(String... args) throws Exception {
        SpringApplication.run(ArrayPrinter.class, args);
    }
}
