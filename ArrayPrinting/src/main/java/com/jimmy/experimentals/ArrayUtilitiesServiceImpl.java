package com.jimmy.experimentals;

import org.springframework.stereotype.Component;

@Component
public class ArrayUtilitiesServiceImpl implements ArrayUtilitiesService {

    @Override
    public String arrayToString(int[] arrayOfNumbers) {

        // String Buffer coz we could be making alotta concatenation
        StringBuffer sb = new StringBuffer();

        sb.append("{");

        for (int i = 0; i < arrayOfNumbers.length; i++) {
            sb.append(arrayOfNumbers[i]);

            // check if it's the last value of the array
            if (i != arrayOfNumbers.length - 1) {
                sb.append(", ");
            }
        }

        sb.append("}");

        return sb.toString();
    }

    @Override
    public boolean contains(int searchValue, int[] arrayOfNumbers) {
        for(int i : arrayOfNumbers) {
            if (i == searchValue) {
                // found value! :D
                return true;
            }
        }
        // did not find value... :(
        return false;
    }

    @Override
    public int search(int searchValue, int[] arrayOfNumbers) {
        for(int i = 0; i < arrayOfNumbers.length; i++) {
            if (arrayOfNumbers[i] == searchValue) {
                return i;
            }
        }
        return -1;
    }

    @Override
    public boolean equals(int[] array1, int[] array2) {
        if (array1.length == array2.length) {
            for(int i = 0; i < array1.length; i++) {
                if (array1[i] != array2[i]) {
                    return false;
                }
            }
            return true;
        } else {
            return false;
        }
    }
}
