package com.jimmy.experimentals;

public interface ArrayUtilitiesService {
    String arrayToString(int[] arrayOfNumbers);

    boolean contains(int searchValue, int[] arrayOfNumbers);

    int search(int i, int[] arrayOfNumbers);

    boolean equals(int[] array1, int[] array2);
}
