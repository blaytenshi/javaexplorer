/**
 * Created by blaytenshi on 15/06/2017.
 */
import React, { Component } from 'react';
import ReactDOM from 'react-dom';

class App extends Component {
    render() {
        return (
            <div>Hello World!</div>
        );
    }
}

ReactDOM.render(<App />, document.querySelector('.container'));