import com.jimmy.experimentals.ReadMapper;

/**
 * Created by jtang on 19/6/17.
 */
public class MainGenerics {

    public static void main(String... args) {

        ReadMapper<TypeA> readmapper = new ReadMapper<>();

        // readMapper.getMappedString();

    }

    public static enum TypeA {
        STRING_A(0, 5),
        STRING_B(5, 5);

        private int position;
        private int numToRead;

        TypeA(int position, int numToRead) {
            this.position = position;
            this.numToRead = numToRead;
        }
    }

    public static enum TypeB {
        STRING_A(0, 2),
        STRING_B(0, 2),
        STRING_C(0, 2),
        STRING_D(0, 2),
        STRING_E(0, 2);

        private int position;
        private int numToRead;

        TypeB(int position, int numToRead) {
            this.position = position;
            this.numToRead = numToRead;
        }
    }

}
