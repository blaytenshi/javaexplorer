package com.jimmy.experimentals.unwrapping;

/**
 * Created by jimmy.tang on 7/07/2017.
 */
public class Unwrapping {

    public static void main(String... args) throws IllegalAccessException, NoSuchFieldException {

        Box box = new Box(10, 15, 20L);

        System.out.println("Box size value prior to unmapping values: " + box.size);
        System.out.println("Box idNumber value prior to unmapping values: " + box.idNumber);
        System.out.println("Box longNumber value prior to unmapping values: " + box.longNumber);

        FieldMapper mapper = new FieldMapper(box);
        mapper.unbox();

        System.out.println("Box size value post unmapping: " + box.size);
        System.out.println("Box idNumber value post unmapping: " + box.idNumber);
        System.out.println("Box longNumber vlaue post unmapping: " + box.longNumber);

    }

}
