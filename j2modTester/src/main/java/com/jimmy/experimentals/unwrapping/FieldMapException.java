package com.jimmy.experimentals.unwrapping;

/**
 * Created by jimmy.tang on 10/07/2017.
 */
public class FieldMapException extends Exception {

    private static final long serialVersionUID = 1L;

    public FieldMapException() {
        super();
    }
    public FieldMapException(String message)
    {
        super(message);
    }

    public FieldMapException(Exception cause)
    {
        super(cause);
    }

    public FieldMapException(String message, Exception cause)
    {
        super(message, cause);
    }
}
