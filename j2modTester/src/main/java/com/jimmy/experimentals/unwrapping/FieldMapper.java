package com.jimmy.experimentals.unwrapping;

import java.lang.reflect.Field;

public class FieldMapper {

    private final Object instance;
    private final Class<? extends Object> instanceClass;
    private final Field field1;
    private final Field field2;
    private final Field field3;

    public FieldMapper(Object instance) throws NoSuchFieldException {
        this.instance = instance;
        this.instanceClass = this.instance.getClass();
        this.field1 = this.instanceClass.getDeclaredField("size");
        this.field2 = this.instanceClass.getDeclaredField("idNumber");
        this.field3 = this.instanceClass.getDeclaredField("longNumber");
    }

    public void unbox() throws IllegalAccessException {

        byte[] numberByteArray = new byte[8];

        numberByteArray[0] = (byte) 0xFF;
        numberByteArray[1] = (byte) 0xFF;
        numberByteArray[2] = (byte) 0xFF;
        numberByteArray[3] = (byte) 0xFF;
        numberByteArray[4] = (byte) 0xFF;
        numberByteArray[5] = (byte) 0xFF;
        numberByteArray[6] = (byte) 0xFF;
        numberByteArray[7] = (byte) 0xFF;

        try {
            field1.set(this.instance, this.convertNumber(ModbusRegisterDataType.INT16, field1.getType(), numberByteArray));
            field2.set(this.instance, this.convertNumber(ModbusRegisterDataType.UINT16, field2.getType(), numberByteArray));
            field3.set(this.instance, this.convertNumber(ModbusRegisterDataType.UINT32, field3.getType(), numberByteArray));
        } catch(FieldMapException e) {
            e.printStackTrace();
        }
    }

    private Object convertNumber(ModbusRegisterDataType modbusRegisterDataType, Class<?> fieldType, byte[] numberByteArray) throws FieldMapException {

        if (modbusRegisterDataType.equals(ModbusRegisterDataType.INT16) && (fieldType == int.class || fieldType == Integer.class)) {
            short value = (short) ((numberByteArray[0] & 0xFF) << 8 | (numberByteArray[1] & 0xFF));
            return value; // returns an int
        } else if (modbusRegisterDataType.equals(ModbusRegisterDataType.UINT16) && (fieldType == int.class || fieldType == Integer.class)) {
            short value = (short) ((numberByteArray[0] & 0xFF) << 8 | (numberByteArray[1] & 0xFF));
            return Short.toUnsignedInt(value);
        } else if (modbusRegisterDataType.equals(ModbusRegisterDataType.UINT32) && (fieldType == long.class || fieldType == Long.class)) {
            int value = (numberByteArray[0] & 0xFF) << 24 | (numberByteArray[1] & 0xFF) << 16 | (numberByteArray[2] & 0xFF) << 8 | (numberByteArray[3] & 0xFF);
            return Integer.toUnsignedLong(value);
        } else if (modbusRegisterDataType.equals(ModbusRegisterDataType.ACC32) && (fieldType == long.class || fieldType == Long.class)) {
            // Accumulated value (32bit). Is used for all sequentially increasing values. NaN Value = 0x00000000
            int value = (numberByteArray[0] & 0xFF) << 24 | (numberByteArray[1] & 0xFF) << 16 | (numberByteArray[2] & 0xFF) << 8 | (numberByteArray[3] & 0xFF);
            return Integer.toUnsignedLong(value);
        } else if (modbusRegisterDataType.equals(ModbusRegisterDataType.STRING16) || modbusRegisterDataType.equals(ModbusRegisterDataType.STRING32)) {
            return new String(numberByteArray).trim();
        } else {
            throw new FieldMapException("Mismatch in given field type and converted type");
        }
    }

}
