package com.jimmy.experimentals.unwrapping;

/**
 * Created by jimmy.tang on 7/07/2017.
 */
public class Box {

    int size;
    Integer idNumber;
    long longNumber;

    public Box(int size, int idNumber, long longNumber) {
        this.size = size;
        this.idNumber = idNumber;
        this.longNumber = longNumber;
    }
}
