package com.jimmy.experimentals.unwrapping;

/**
 * Created by jimmy.tang on 27/06/2017.
 */
public enum ModbusRegisterDataType
{
    INT16,
    UINT16,
    UINT32,
    STRING16,
    STRING32,
    ACC32 // SunSpec Data Types Description: Accumulated value (32 bit). Is used for all sequentially increasing values.
}
