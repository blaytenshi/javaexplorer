package com.jimmy.experimentals;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;

/**
 * Created by jimmy.tang on 26/06/2017.
 */
public class ByteBufferTest {
    public static void main(String[] args) {
        byte[] payload = toArray(1400204883);
        int number = fromArray(payload);
        System.out.println(number);
    }

    public static  int fromArray(byte[] payload){
        ByteBuffer buffer = ByteBuffer.wrap(payload);
        buffer.order(ByteOrder.BIG_ENDIAN);
        return buffer.getInt();
    }

    public static byte[] toArray(int value){
        ByteBuffer buffer = ByteBuffer.allocate(4);
        buffer.order(ByteOrder.BIG_ENDIAN);
        buffer.putInt(value);
        buffer.flip();
        return buffer.array();
    }
}
