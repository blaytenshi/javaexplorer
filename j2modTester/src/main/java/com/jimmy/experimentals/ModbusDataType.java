package com.jimmy.experimentals;

/**
 * Created by jimmy.tang on 27/06/2017.
 */
public enum ModbusDataType {
    UINT32,
    UINT16,
    STRING32,
    STRING16,
    INT16,
    ACC32
}
