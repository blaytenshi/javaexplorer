package com.jimmy.experimentals;

/**
 * Created by jimmy.tang on 21/06/2017.
 */
public enum SmiRegisterDatafield implements ModbusRegisterDatafield {
    COM_C_SUNSPEC_ID(0, 2, ModbusDataType.UINT32),
    COM_C_SUNSPEC_DID(2, 1, ModbusDataType.UINT16),
    COM_C_SUNSPEC_LENGTH(3, 1, ModbusDataType.UINT16),
    COM_C_MANUFACTURER(4, 16, ModbusDataType.STRING32),
    COM_C_MODEL(20, 16, ModbusDataType.STRING32),
    COM_C_VERSION(44, 8, ModbusDataType.STRING16),
    COM_C_SERIALNUMBER(52, 16, ModbusDataType.STRING32),
    COM_C_DEVICEADDRESS(68, 1, ModbusDataType.UINT16);
    /*
    ,
    INV_C_SUNSPEC_DID(69, 1, ModbusDataType.UINT16),
    INV_C_SUNSPEC_LENGTH(70, 1, ModbusDataType.UINT16),
    INV_I_AC_CURRENT(71, 1, ModbusDataType.UINT16),
    INV_I_AC_CURRENTA(72, 1, ModbusDataType.UINT16),
    INV_I_AC_CURRENTB(73, 1, ModbusDataType.UINT16),
    INV_I_AC_CURRENTC(74, 1, ModbusDataType.UINT16),
    INV_I_AC_CURRENT_SF(75, 1, ModbusDataType.INT16),
    INV_I_AC_VOLTAGEAB(76, 1, ModbusDataType.UINT16),
    INV_I_AC_VOLTAGEBC(77, 1, ModbusDataType.UINT16),
    INV_I_AC_VOLTAGECA(78, 1, ModbusDataType.UINT16),
    INV_I_AC_VOLTAGEAN(79, 1, ModbusDataType.UINT16),
    INV_I_AC_VOLTAGEBN(80, 1, ModbusDataType.UINT16),
    INV_I_AC_VOLTAGECN(81, 1, ModbusDataType.UINT16),
    INV_I_AC_VOLTAGE_SF(82, 1, ModbusDataType.INT16),
    INV_I_AC_POWER(83, 1, ModbusDataType.INT16),
    INV_I_AC_POWER_SF(84, 1, ModbusDataType.INT16),
    INV_I_AC_FREQUENCY(85, 1, ModbusDataType.UINT16),
    INV_I_AC_FREQUENCY_SF(86, 1, ModbusDataType.INT16),
    INV_I_AC_VA(87, 1, ModbusDataType.INT16),
    INV_I_AC_VA_SF(88, 1, ModbusDataType.INT16),
    INV_I_AC_VAR(89, 1, ModbusDataType.INT16),
    INV_I_AC_VAR_SF(90, 1, ModbusDataType.INT16),
    INV_I_AC_PF(91, 1, ModbusDataType.INT16),
    INV_I_AC_PF_SF(92, 1, ModbusDataType.INT16),
    INV_I_AC_ENERGY_WH(93, 2, ModbusDataType.ACC32),
    INV_I_AC_ENERGY_WH_SF(95, 1, ModbusDataType.UINT16),
    INV_I_DC_CURRENT(96, 1, ModbusDataType.UINT16),
    INV_I_DC_CURRENT_SF(97, 1, ModbusDataType.INT16),
    INV_I_DC_VOLTAGE(98, 1, ModbusDataType.UINT16),
    INV_I_DC_VOLTAGE_SF(99, 1, ModbusDataType.INT16),
    INV_I_DC_POWER(100, 1, ModbusDataType.INT16),
    INV_I_DC_POWER_SF(101, 1, ModbusDataType.INT16),
    INV_I_TEMP_SINK(102, 1, ModbusDataType.INT16),
    INV_I_TEMP_SF(106, 1, ModbusDataType.INT16),
    INV_I_STATUS(107, 1, ModbusDataType.UINT16),
    INV_I_STATUS_VENDOR(108, 1, ModbusDataType.UINT16),
    INV_I_EVENT_1(109, 2, ModbusDataType.UINT32),
    INV_I_EVENT_2(111, 2, ModbusDataType.UINT32),
    INV_I_EVENT_1_VENDOR(113, 2, ModbusDataType.UINT32),
    INV_I_EVENT_2_VENDOR(115, 2, ModbusDataType.UINT32),
    INV_I_EVENT_3_VENDOR(117, 2, ModbusDataType.UINT32),
    INV_I_EVENT_4_VENDOR(119, 2, ModbusDataType.UINT32);
    */

    private final int startPosition;
    private final int numToRead;
    private final ModbusDataType dataType;

    SmiRegisterDatafield(int startPosition, int numToRead, ModbusDataType dataType) {
        this.startPosition = startPosition;
        this.numToRead = numToRead;
        this.dataType = dataType;
    }

    public int getStartPosition() {
        return startPosition;
    }

    public String getName() {
        return "";
    }

    public int getNumToRead() {
        return numToRead;
    }

    public ModbusDataType getDataType() {
        return dataType;
    }
}
