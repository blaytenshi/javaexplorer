package com.jimmy.experimentals;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by jimmy.tang on 19/06/2017.
 */
public class ArraySeperator {

    public static void main(String... args) {
        String[] strings = new String[13];

        strings[0] = "ab";
        strings[1] = "cd";
        strings[2] = "ef";
        strings[3] = "gh";
        strings[4] = "ij";
        strings[5] = "kl";
        strings[6] = "mn";
        strings[7] = "op";
        strings[8] = "qr";
        strings[9] = "st";
        strings[10] = "uv";
        strings[11] = "wx";
        strings[12] = "yz";

        Map<Word, String> map = new HashMap<>();

        for (Word word : Word.values()) {
            String s = "";
            //           0      2
            //           1      2
            for (int i = 0; i < word.numToRead; i++) {
                //              0
                //              1
                s = s + strings[word.position + i];
            }
            map.put(word, s);
        }

        for (Word word : map.keySet()) {
            System.out.println(map.get(word));
        }
        
    }

    public static enum Word {
        FIRST(0, 2),
        SECOND(2, 2),
        THIRD(4, 2),
        FOURTH(6, 4),
        FIFTH(10, 3);

        public final int position;
        public final int numToRead;

        private Word(int position, int numToRead) {
            this.position = position;
            this.numToRead = numToRead;
        }
    }

}
