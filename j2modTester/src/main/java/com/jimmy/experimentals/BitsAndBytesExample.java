package com.jimmy.experimentals;

/**
 * Bits and Bytes in Java
 *
 * Java has a number of primitive values. Several of them can be used to represent binary data: byte, short, int,
 * and long. Each hold a different LENGTH of bits. Understanding how to utilise them is important to understanding
 * how to process binary information from low level systems (eg Modbus Registers).
 *
 * Definitions:
 * 2's (Two's) Complement
 * -----
 * A mathematical operation on binary numbers as well as a way of representing signed binary numbers where the most
 * significant bit is used to denote a negative value. Used in most modern computer systems today. Also the way Java
 * represents numbers.
 * More Information: https://stackoverflow.com/questions/11054213/advantage-of-2s-complement-over-1s-complement
 *
 * Signed/Unsigned Primitive Types
 * -----
 * Unsigned types means the primitive type only allow positive values in its binary representation.
 * Signed types allow for negative values in it's binary representation. How the negative values are represented
 * depends on the representation system (eg, 2's Complement, 1's Complement, Base -2, etc)
 * More information: https://en.wikipedia.org/wiki/Signed_number_representations
 *
 * Bit
 * -----
 * A single 0/1 value. Eg: '1', '0'.
 *
 * Byte (Generic)
 * -----
 * A group of 8 0/1 values. Eg: '00001111', '11111111', '01010101'.
 *
 * Byte (Java primitive)
 * -----
 * The smallest grouping of bits the Java language allows. In Java, the byte is grouped in the same fashion as its
 * generic byte, 8 bits. However, the Java byte is represented in 2's complement form. Meaning the smallest and largest
 * number this data-type can represent is -128 to 127 (inclusive) or -2^7 to ((2^7)-1).
 *
 * Short (Java primitive)
 * -----
 * This grouping of bits in the Java language allows 16 bits. Also represented in 2's complement form. The smallest
 * number this data type can represent is -32,768 to 32,767 (inclusive) or -2^15 to ((2^16)-1).
 *
 * Int (Java primitive)
 * -----
 * This grouping of bits in the Java language allows 32 bits. Also represented in 2's complement form. The smallest
 * number this data type can represent is -2,147,483,648 to 2,147,483,647 (inclusive) or -2^31 to ((2^31)-1).
 *
 * Long (Java primitive)
 * -----
 * This grouping of bits in the Java language allows 64 bits. Also represented in 2's complement form. The smallest
 * number this data type can represent is -9,223,372,036,854,775,808 to 9,223,372,036,854,775,807 (inclusive) or -2^63
 * to ((2^63)-1).
 */

public class BitsAndBytesExample {

    public static void main(String... args) {

        // Number literals
        // - All number literals are automatically cast into 32-bit int types unless specifically recast
        // - For numbers larger than int, you need to add an L at the end to it to specify long type
        // - Literals can be specified in 4 different forms; binary, hex, octal, decimal
        byte byteBinary   = (byte)  0b11111111;
        short shortBinary = (short) 0b1111111111111111;
        int intBinary     =         0b11111111111111111111111111111111;
        long longBinary   =         0b1111111111111111111111111111111111111111111111111111111111111111L;

        // Signed and unsigned forms
        // As we know, Java primitives are in signed form with the highest most significant bit being used to
        // represent positive/negative value in 2's compliment binary representation.
        // We can choose to read the values in signed and unsigned forms simply by calling the relevant method on the value.
        System.out.println("Value of 11111111 (byte) in signed form: " + Byte.valueOf(byteBinary)); // java default value in signed form is -1 (in decimal integer)
        System.out.println("Value of 11111111 (byte) in unsigned form: " + Byte.toUnsignedInt(byteBinary)); // convert to unsigned form using jdk8+ is 255
        System.out.println("Value of 11111111 11111111 (short) in signed form: " + Short.valueOf(shortBinary));
        System.out.println("Value of 11111111 11111111 (short) in unsigned form: " + Short.toUnsignedInt(shortBinary));
        System.out.println("Value of 11111111 11111111 11111111 11111111 (int) in signed form: " + Integer.valueOf(intBinary));
        System.out.println("Value of 11111111 11111111 11111111 11111111 (int) in signed form: " + Integer.toUnsignedLong(intBinary));
        System.out.println("Value of 11111111 11111111 11111111 11111111 11111111 11111111 11111111 11111111 (long) in signed form: " + Long.valueOf(longBinary));
        System.out.println("Value of 11111111 11111111 11111111 11111111 11111111 11111111 11111111 11111111 (long) in unsigned form: " + Long.toUnsignedString(longBinary));

        // Narrowing Conversion and Widening Conversion
        short shortThatFits = (short) 0b0000111111111111;
        byte narrowShortToByte = (byte) (shortThatFits); // Narrowing conversion from 16 bits to 8 bits and it becomes 0b11111111 (extra 00001111 cut off)
        short widenByteToShort = (short) (narrowShortToByte & 0xFF); // Widening conversion 8 bits back to 16 bits and it becomes 0b1111111111111111 (the original 00001111 data is lost and sign bit is extended to pad the extra bits)
        System.out.println("Value of original shortThatFits: " + Short.toUnsignedInt(shortThatFits));
        System.out.println("Value of narrowShortToByte: " + Byte.toUnsignedInt(narrowShortToByte));
        System.out.println("Value of widenByteToShort: " + Short.toUnsignedInt(widenByteToShort));

        // Byte Arrays and Bit-Shifting
        // Most low level data is represented in bytes (8 bits) therefore we usually push them into a byte array to
        // process them. Below we can see a byte array of 4 bytes. Each byte in the array contains the same value.
        // Remember, all numbers are converted to int primitives in java so we need to cast them to byte types.
        byte buf[] = new byte[4];

        buf[0] = (byte) 0b11111110;
        buf[1] = (byte) 0b11111110;
        buf[2] = (byte) 0b11111110;
        buf[3] = (byte) 0b11111110;

        int con = buf[1] | ((int)buf[0]) << 8;
        int cont = (buf[0] & 0xFF) << 24 |
                   (buf[1] & 0xFF) << 16 |
                   (buf[2] & 0xFF) << 8  |
                   (buf[3] & 0xFF);
        System.out.println("con in binary: " + Integer.toBinaryString(con));
        System.out.println("con in hex: " + Integer.toHexString(con));
        System.out.println("cont in binary: " + Integer.toBinaryString(cont));
        System.out.println("buf[] from function: " + processByteArray(buf));
        System.out.println("buf[] from function to unsigned long: " + Integer.toUnsignedLong(processByteArray(buf)));

        /////

        int zero = 0;
        zero = buf[3] << 24;
        System.out.println("zero in binary: " + Integer.toBinaryString(zero));

        /////
        // What does 0xff do in Java? https://stackoverflow.com/questions/11380062/what-does-value-0xff-do-in-java
        byte byte1 = (byte) 0x70; // 01110000
        byte byte2 = (byte) 0xff; // 11111111

        int value = (byte1 & 0xff) << 8 | (byte2 & 0xff);
        System.out.println(Integer.toBinaryString(value));

    }

    public static int processByteArray(byte[] byteArray)
    {
        int value = 0;
        if (byteArray.length == 2)
        {
            value = (byteArray[0] & 0xFF) << 8 | (byteArray[1] & 0xFF);
        }
        else if (byteArray.length == 4)
        {
            value = (byteArray[0] & 0xFF) << 24 | (byteArray[1] & 0xFF) << 16 | (byteArray[2] & 0xFF) << 8 | (byteArray[3] & 0xFF);
        }
        return value;
    }

}
