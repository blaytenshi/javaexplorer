package com.jimmy.experimentals;

/**
 * Created by jimmy.tang on 21/06/2017.
 */
public interface ModbusRegisterDatafield {
    String getName();
    int getStartPosition();
    int getNumToRead();
    ModbusDataType getDataType();
}
