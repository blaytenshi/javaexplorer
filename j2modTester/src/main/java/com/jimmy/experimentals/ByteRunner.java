package com.jimmy.experimentals;

/**
 * Created by jtang on 22/6/17.
 */
public class ByteRunner {

    public static void main(String... args) {

//      String byteGeneratorString = "This is a string";
        String byteGeneratorString = "";
        int a = 0x5375;
        int b = 0x6e53;
        int shifted = (a << 16) | b; // 32bit int
        int unsignedInt = shifted & 0xffff;

        System.out.println(Integer.toBinaryString(a));
        System.out.println(Integer.toBinaryString(b));
        System.out.println(Integer.toBinaryString(shifted));
        System.out.println(Integer.toUnsignedString(shifted));


        byte[] intArray = { (byte) 0x5375, (byte) 0x6e53, (byte) 0x5375, (byte) 0x6e53 };


        ByteGenerator byteGen = new ByteGenerator(byteGeneratorString);
        ByteArrayBuilder byteArrayBuilder = new ByteArrayBuilder();

        // -- ByteGenerator Test --
        // Print out the string we put in
        System.out.println(byteGen.getInputString());

        // Print out the byte array converted to int
        byte[] byteArray = byteGen.getByteArrayFromString();
        for(int i = 0; i < byteArray.length; i++) {
            System.out.println(byteArray[i]);
        }

        // -- ByteArrayBuilder Test --
        // Adding byte into array one letter at a time
        byteArrayBuilder.addLetter("a");
        byteArrayBuilder.addLetter("b");

        byte[] stringByteArray = byteArrayBuilder.getByteArray();

        for(int i = 0; i < stringByteArray.length; i++) {
            System.out.println(stringByteArray[i]);
        }

    }

}
