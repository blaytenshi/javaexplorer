package com.jimmy.experimentals;

/**
 * Created by jimmy.tang on 10/07/2017.
 */
public class SignedUnsigned {

    public static void main(String... args) {

        byte byteValue = (byte) 0b11110000;

        // value is -16 in 2's Comp signed binary representation
        System.out.println("Byte: " + byteValue);

        // value is 240 in regular unsigned binary representation
        System.out.println("Byte unsigned: " + Byte.toUnsignedInt(byteValue));

        // Why does it need to be converted to an int?
        // -----
        // Java's primitive value system is always going to be 2's complement so in order to store a value that is
        // 'larger' that what is defined in the primitive range, it must move 'up' to a larger primitive value.
        // See the example of short to int below.

        // An regular unsigned short (8 bits, 2 bytes) can show a maximum value of 65,535.
        // short shortValue = 65535; // You can't do this in java because Java's short is 2's complement signed.
        // This mean the maximum value in Java's short is:
        short shortValue = 32767; // valid code! Java will automatically request you cast anything larger into an int.

        // However, we can still get the value of 65,535 stored in a short if we simply tell Java to read the contents
        // of the value as if it is 'unsigned'. How?
        // First let's construct the short in binary:
        short shortValueInBinary = (short) 0xFFFF; // 0xFFFF is the binary binary equivalent of: 1111 1111 1111 1111
        // CAVEAT: By casting the 0xFFFF into a (short) we're actually allowed to go to Java int's maximum of
        // 0xFFFFFFFF as Java casts all numbers smaller than a long into an int (coz who needs a number as large as
        // 2^64 anyways? That's like 18 quintillion...)

        // Now although we can represent 1111 1111 1111 1111 in 16 bits that fits into a short, we must remember Java's
        // number system are in 2's complement meaning the most significant bit (MSB), the bit all the way to the left
        // is used to denote whether this value is a negative number. A positive 65,535 is actually:
        // 0 1111 1111 1111 1111! The 0 actually overflows into the next bit order. What's the next bit order? That's
        // right! The int type! So we have to tell Java to get value of shortValue in the form of what we know to be
        // an unsigned integer.
        System.out.println("shortValueInBinary unsigned: " + Short.toUnsignedInt(shortValueInBinary));
        // which should give us 65,535.

        // So what would it be in regular Java 2's complement binary?
        System.out.println("shortValueInBinary: " + shortValueInBinary);

    }

}
