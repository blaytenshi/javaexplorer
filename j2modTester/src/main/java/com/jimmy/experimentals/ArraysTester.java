package com.jimmy.experimentals;

import java.util.Arrays;

/**
 * Created by jtang on 22/6/17.
 */
public class ArraysTester {

    public static void main(String... args) {

        int[] intArray = { 1, 2, 3, 4, 5 };

        intArray = Arrays.copyOf(intArray, intArray.length + 1);

        System.out.println(intArray.length);


    }

}
