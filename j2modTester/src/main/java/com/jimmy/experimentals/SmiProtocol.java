package com.jimmy.experimentals;

import com.ghgande.j2mod.modbus.ModbusException;
import com.ghgande.j2mod.modbus.facade.ModbusTCPMaster;

/**
 * Created by jimmy.tang on 14/06/2017.
 */
public class SmiProtocol {

    private String address;
    private int port;
    private int timeout = 8000;
    private int deviceNumber;
    private boolean reconnect = true;
    private ModbusTCPMaster com;

    public SmiProtocol(String address, int port, int deviceNumber) {
        this.address = address;
        this.port = port;
        this.deviceNumber = deviceNumber;
    }

    public boolean connect() {
        if (!isConnected()) {
            try {
                this.com = new ModbusTCPMaster(address, port);
                this.com.connect();
                return true;
            } catch (Exception e) {
                System.out.println("ERR: Connection could not be established");
                e.printStackTrace();
                return false;
            }
        } else {
            // already connected, do nothing
            return false;
        }
    }

    public boolean isConnected() {
        return this.com != null;
    }

    public boolean disconnect() {
        if (isConnected()) {
            this.com.disconnect();
            return true;
        } else {
            // no connection to disconnect from, do nothing
            return false;
        }
    }

    // poll all registers
    public void poll() {
        try {
            com.readMultipleRegisters(deviceNumber, 0, 120);
        } catch (ModbusException e) {
            System.out.println("ERR: Unable to read from specified device.");
            System.out.println(e.getMessage());
            e.printStackTrace();
        }
    }

    // poll specific register
    public void poll(int reference, int bytesToRead, String type) {
        try {
            com.readMultipleRegisters(deviceNumber, bytesToRead);
        } catch (ModbusException e) {
            System.out.println("ERR: Unable to read from specified device or reference.");
            System.out.println(e.getMessage());
            e.printStackTrace();
        }
    }

}
