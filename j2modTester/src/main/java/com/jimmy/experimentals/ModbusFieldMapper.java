package com.jimmy.experimentals;

import com.ghgande.j2mod.modbus.procimg.Register;

import java.lang.reflect.Field;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

public class ModbusFieldMapper {

    private final Object instance;

    private final Class<? extends Object> instanceClass;

    private final Map<ModbusRegisterDatafield, Field> mapping;

    private final Map<String, ModbusRegisterDatafield> nameMapping;

    public ModbusFieldMapper(Object inst) {
        this.instance = inst;
        this.instanceClass = this.instance.getClass();
        this.nameMapping = new HashMap<>();
        this.mapping = new HashMap<>();
    }

    public void addMapping(String name, ModbusRegisterDatafield datafield) {
        try {
            Field field = this.instanceClass.getDeclaredField(name);
            if (!field.isAccessible()) {
                field.setAccessible(true);
            }
            this.mapping.put(datafield, field);
            this.nameMapping.put(field.getName(), datafield);

        } catch (NoSuchFieldException e) {
            e.printStackTrace();
        }
    }

    public void unrollModbusRegisters(Register[] registerArray) {

        for(ModbusRegisterDatafield datafield : this.nameMapping.values()) {

            // setup variables
            int start = datafield.getStartPosition();
            int posToRead = datafield.getNumToRead();
            byte[] datafieldValueArray;

            if (posToRead > 1) {
                // if data is contained in more than one register, extract register range into byte array and process
                datafieldValueArray = this.registerArrayToByteArray(Arrays.copyOfRange(registerArray, start, start+posToRead));

            } else {
                // if data is in only one register, process that single register
                datafieldValueArray = registerArray[start].toBytes();
            }

            try {
                this.setAttribute(this.mapping.get(datafield), datafield, datafieldValueArray);
            } catch (Exception e) {
                e.printStackTrace();
                System.out.println("Cannot set data field");
            }
        }
    }

    // converts Register[] array into byte array
    private byte[] registerArrayToByteArray(Register[] registerArray) {

        byte[] byteArray = new byte[registerArray.length * 2]; // Each register has two bytes in big-endian format
        int registerArrayCount = 0;

        for(int i = 0; i < byteArray.length; i = i + 2) {
            byte[] tempArray = registerArray[registerArrayCount].toBytes();
            System.arraycopy(tempArray, 0, byteArray, i, 2);
            registerArrayCount++;
        }

        return byteArray;
    }

    private void setAttribute(Field field, ModbusRegisterDatafield datafield, byte[] value) throws Exception {
        try
        {
            if (value == null || value.length == 0)
            {
                /* Special case, all types can be null. */
                field.set(this.instance, value);
                return;
            }

            Class<?> valueType = value.getClass();

            field.set(this.instance, this.convertType(datafield.getDataType(), value));
        }
        catch (IllegalArgumentException ex)
        {
            System.out.println("Failed to set data filed " + field + ", incompatible value type.");
            throw new Exception(ex);
        }
        catch (IllegalAccessException ex)
        {
            System.out.println("Failed to set data field " + field + ", unable to access data field.");
            throw new Exception(ex);
        }
    }

    private Object convertType(ModbusDataType modbusDataType, byte[] value) {
        if (modbusDataType.equals(ModbusDataType.UINT16) || modbusDataType.equals(ModbusDataType.UINT32)) {
            return getIntFromByteArray(value);
        } else if (modbusDataType.equals(ModbusDataType.STRING16) || modbusDataType.equals(ModbusDataType.STRING32)) {
            return new String(value).trim();
        }
        return null;
    }

    private int getIntFromByteArray(byte[] byteArray) {
        ByteBuffer byteBuffer = ByteBuffer.wrap(byteArray);
        byteBuffer.order(ByteOrder.BIG_ENDIAN);
        return byteBuffer.getInt();
    }

}
