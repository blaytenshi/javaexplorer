package com.jimmy.experimentals;

/**
 * Created by jtang on 22/6/17.
 */
public class ByteGenerator {

    private String string;
    private byte[] stringByteArray;

    public ByteGenerator(String s) {
        this.string = s;
        this.stringByteArray = s.getBytes();
    }

    public String getInputString() {
        return string;
    }

    public byte[] getByteArrayFromString() {
        return stringByteArray;
    }

}
