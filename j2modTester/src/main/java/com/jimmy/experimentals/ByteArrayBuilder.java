package com.jimmy.experimentals;

import java.util.Arrays;

/**
 * Created by jtang on 22/6/17.
 */
public class ByteArrayBuilder {

    private byte[] byteArray;

    public ByteArrayBuilder() {
        this.byteArray = new byte[0];
    }

    public void addLetter(String string) {

        byteArray = Arrays.copyOf(byteArray, byteArray.length + 1);
        System.out.println(byteArray.length);
        byteArray[byteArray.length - 1] = (byte) string.charAt(0);

    }

    public byte[] getByteArray() {
        return byteArray;
    }
}
