package com.jimmy.experimentals;

import com.ghgande.j2mod.modbus.facade.ModbusTCPMaster;
import com.ghgande.j2mod.modbus.procimg.Register;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.util.*;

public class Master {

    private static ModbusTCPMaster master;
    private static String address = "172.22.111.197";
    private static int port = 502;
    private static int timeout = 8000;
    private static boolean reconnect = true;
    private static Map<SmiRegisterDatafield, String> mapping;

    public static void main(String[] args) {

        try {
            // Create Modbus Connection object and connect
            master = new ModbusTCPMaster(address, port, timeout, reconnect);
            master.connect();

            // Set up mappings map
            mapping = new HashMap<>();
            for(SmiRegisterDatafield register : SmiRegisterDatafield.values()) {
                mapping.put(register, "");
            }

            // Read Registers
            Register[] reg = master.readMultipleRegisters(2,0, 123);

            // Print Registers
            for(int i = 0; i < 123; i++) {
                //System.out.println(i + " : " + Arrays.toString(reg[i].toBytes()));
                //System.out.println(i + " : " + Integer.toHexString(reg[i].getValue()));
                //System.out.println(i + " : " + reg[i].getValue());
            }

            // Print out the contents of the mapped register
            for(ModbusRegisterDatafield datafield : mapping.keySet()) {

                // setup variables
                int start = datafield.getStartPosition();
                int posToRead = datafield.getNumToRead();

                if (posToRead > 1) {
                    // if data is contained in more than one register, extract register range into byte array and process
                    byte[] byteArray = registerArrayToByteArray(Arrays.copyOfRange(reg, start, start+posToRead));

                    if (datafield.getDataType().equals(ModbusDataType.STRING32) || datafield.getDataType().equals(ModbusDataType.STRING16)) {
                        System.out.println(new String(byteArray).trim());
                    } else if (datafield.getDataType().equals(ModbusDataType.UINT32)) {
                        System.out.println(Integer.toUnsignedString(byteArrayToInteger(byteArray)));
                    }

                } else {
                    // if data is in only one register, process that single register
                    int value = reg[start].getValue();
                    System.out.println(datafield.getName() + " : " + value);
                }

            }

        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (master != null) {
                master.disconnect();
            }
        }

    }

    public static byte[] registerArrayToByteArray(Register[] registerArray) {

        byte[] byteArray = new byte[registerArray.length * 2];
        int registerArrayCount = 0;

        for(int i = 0; i < byteArray.length; i = i + 2) {
            byte[] tempArray = registerArray[registerArrayCount].toBytes();
            System.arraycopy(tempArray, 0, byteArray, i, 2);
            registerArrayCount++;
        }

        /*
        // uncomment for debugging
        for(int i = 0; i < byteArray.length; i++) {
            System.out.println(byteArray[i]);
        }
        */

        return byteArray;
    }

    public static int byteArrayToInteger(byte[] byteArray) {
        ByteBuffer byteBuffer = ByteBuffer.wrap(byteArray);
        byteBuffer.order(ByteOrder.BIG_ENDIAN);
        return byteBuffer.getInt();
    }
}
