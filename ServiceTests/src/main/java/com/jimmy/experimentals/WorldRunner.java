package com.jimmy.experimentals;

public class WorldRunner {

    private final BoxService boxService;

    public WorldRunner(BoxService boxService) {
        this.boxService = boxService;
    }

    public void create() {
        boxService.createBox();
    }

}
