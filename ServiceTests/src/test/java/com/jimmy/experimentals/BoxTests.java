package com.jimmy.experimentals;

import org.junit.Test;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.MatcherAssert.assertThat;

public class BoxTests {

    @Test
    public void shouldReturnXWhenGetXIsCalled() throws Exception {
        Box box = new Box();

        assertThat(box.getX(), equalTo(1));
        assertThat(box.getY(), equalTo(1));
        assertThat(box.getX(), equalTo(2));
        assertThat(box.getY(), equalTo(2));
    }
}
