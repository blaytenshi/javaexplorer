package com.jimmy.experimentals;

import org.junit.Before;
import org.junit.Test;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

public class WorldRunnerTests {

    /**
     *
     * Every Test Structure:
     *
     * 1) Create any required mock objects
     * 2) Create any real objects, including target objects
     * 3) Specify how you expect the mock objects to be called by the target object
     * 4) Call the triggering method(s) on the target object
     * 5) Assert that any resulting values are valid and that all the expected calls have been made
     *
     */

    private WorldRunner worldRunner;
    private BoxService mockBoxService;

    @Before
    public void setUp() throws Exception {
        mockBoxService = mock(BoxService.class);
        worldRunner = new WorldRunner(mockBoxService);
    }

    @Test
    public void shouldReturnBoxWhenCreateIsCalled() throws Exception {
        when(mockBoxService.createBox()).thenReturn(new Box());

        worldRunner.create();

        verify(mockBoxService).createBox();
    }

}
