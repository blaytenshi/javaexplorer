package com.jimmy.experimentals;

import org.junit.Test;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.junit.Assert.assertThat;

public class BoxServiceTests {
    @Test
    public void shouldReturnBoxInPositionOneByOneWhenCreateBoxIsCalled() {
        BoxService boxService = new BoxServiceImpl();

        Box box = boxService.createBox();

        assertThat(box, equalTo(new Box()));
    }
}
