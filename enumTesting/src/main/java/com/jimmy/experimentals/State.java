package com.jimmy.experimentals;

public enum State {

    INITIAL {
        @Override
        State doSomething(String parameter) {
            return INITIAL;
        }
    };

    abstract State doSomething(String parameter);

}
