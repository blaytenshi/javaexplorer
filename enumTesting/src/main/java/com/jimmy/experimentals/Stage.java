package com.jimmy.experimentals;

public enum Stage {

    FIRST {
        @Override
        public Stage nextStage() {
            return SECOND;
        }
    },
    SECOND {
        @Override
        public Stage nextStage() {
            return this;
        }
    };

    public abstract Stage nextStage();

}
