package com.jimmy.experimentals;

public class StageResolver {

    private Stage stage;

    public StageResolver() {
        this.stage = Stage.FIRST;
    }

    public Stage getStage() {
        return stage.nextStage();
    }

}
