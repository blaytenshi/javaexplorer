package com.jimmy.experimentals;

import javax.swing.*;

/**
 * Created by jtang on 21/2/17.
 */
public class Joption {

    private static int x;

    private static boolean isEven(int x) {
        if (x % 2 == 0) {
            return true;
        }
        return false;
    }

    private static boolean isSingle(int x) {
        if (x >= 0 || x < 10) {
            return true;
        }
        return false;
    }

    private static void isEvenAndSingle(int x) {
        if (isEven(x) && isSingle(x)) {
            JOptionPane.showMessageDialog(null, "Creative message!");
        }
    }

    private static void isEvenAndNotSingle(int x) {
        if (isEven(x) && !(isSingle(x))) {
            JOptionPane.showMessageDialog(null, "Creative Message!");
        }
    }

    public static void main(String[] args) {
        x = Integer.parseInt(JOptionPane.showInputDialog(null, "What is your favorite number?"));
        isEvenAndSingle(x);
        isEvenAndNotSingle(x);
    }

}
