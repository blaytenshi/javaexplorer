package com.jimmy.experimentals;

public class Main {

    public static void main(String... args) {

        for (Operation op : Operation.values()) {
            System.out.println(op.name());
        }

        System.out.println(Operation.PLUS.eval(10, 10));

        StageResolver stageResolver = new StageResolver();

        System.out.println(stageResolver.getStage());

    }

}
