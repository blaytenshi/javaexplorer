package com.company;

public class Main {

    public static void main(String[] args) {

        String s = "blah blah boundary=\"myboundary\"";
        String searchString = "boundary=";
        String boundary;

        // Should return 10 because b in boundary= starts at the 10th slot of the string array
        System.out.println(s.indexOf(searchString));

        boundary = "--" + s.substring(s.indexOf(searchString)).replaceAll("\"", "");
        System.out.println(boundary);
    }
}
