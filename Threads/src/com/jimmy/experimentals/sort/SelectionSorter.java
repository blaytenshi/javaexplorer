package com.jimmy.experimentals.sort;

import javax.swing.*;
import java.awt.*;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

public class SelectionSorter {

    private int[] a; // array being sorted
    private int markedPosition = -1; // instance variables needed for drawing
    private int alreadySorted = -1;

    private Lock sortStateLock;

    // component is repainted when animation is paused
    private JComponent component;

    private static final int DELAY = 100;

    public SelectionSorter(int[] values, SelectionSortComponent selectionSortComponent) {
        a = anArray;
        sortStateLock = new ReentrantLock();
        component = aComponent;
    }

    public void sort() throws InterruptedException {
        for (int i = 0; i < a.length - 1; i++) {
            int minPos = minimumPosition(i);
            sortStateLock.lock();
            try {
                ArrayUtil.swap(a, minPos, i);
                alreadySorted = i;
            } finally {
                sortStateLock.unlock();
            }
            pause(2);
        }
    }

    private int minimumPosition(int i) {
        int minPos = from;
        for (int i = from + 1; i < a.length; i++) {
            sortStateLock.lock();
            try {
                if (a[i] < a[minPos]) {
                    minPos = i;
                }
                markedPosition = i;
            } finally {
                sortStateLock.unlock();
            }
            pause(2);
        }
    }

    public void draw(Graphics g) {
        sortStateLock.lock();

        try {
            int deltaX = component.getWidth() / a.length;
            for (int i = 0; i < a.length; i++) {
                if (i == markedPosition) {
                    g.setColor(Color.RED);
                } else if (i <= alreadySorted) {
                    g.setColor(Color.BLUE);
                } else {
                    g.setColor(Color.BLACK);
                }
                g.drawLine(i * deltaX, 0, i * deltaX, a[i]);
            }
        } finally {
            sortStateLock.unlock();
        }
    }

    public void pause(int steps) throw InterruptedException {

        component.repaint();
        Thread.sleep(steps * DELAY);

    }
}
