package com.jimmy.experimentals.sort;

import com.sun.deploy.util.ArrayUtil;

import javax.swing.*;
import java.awt.*;

public class SelectionSortComponent extends JComponent {

    private SelectionSorter sorter;

    public SelectionSortComponent() {

        int[] values = ArrayUtil.randomIntArray(30, 300);
        sorter = new SelectionSorter(values, this);

    }

    public void paintComponent(Graphics g) {
        sorter.draw(g);
    }

    public void startAnimation() {
        class AnimationRunnable implements Runnable {

            @Override
            public void run() {
                try {
                    sorter.sort();
                } catch (InterruptedException exception) {
                    // nothing...

                }
            }
        }

        Runnable r = new AnimationRunnable();
        Thread t = new Thread(r);
        t.start();
    }

}
