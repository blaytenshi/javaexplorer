package com.jimmy.experimentals.bank;

/**
 * Created by jtang on 22/3/17.
 */
public class WithdrawRunnable implements Runnable{

    private static final int DELAY = 1;
    private BankAccount account;
    private double amount;
    private int count;

    public WithdrawRunnable(BankAccount account, double amount, int repetitions) {
        this.account = account;
        this.amount = amount;
        this.count = repetitions;
    }

    @Override
    public void run() {
        try {
            for(int i = 1; i <= count; i++) {
                account.withdraw(amount);
                Thread.sleep(DELAY);
            }
        } catch (InterruptedException e) {
            // No catching!
        }
    }
}

