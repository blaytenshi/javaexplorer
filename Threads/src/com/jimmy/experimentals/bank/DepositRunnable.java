package com.jimmy.experimentals.bank;

public class DepositRunnable implements Runnable{

    private static final int DELAY = 1;
    private BankAccount account;
    private double amount;
    private int count;

    public DepositRunnable(BankAccount account, double amount, int repetitions) {
        this.account = account;
        this.amount = amount;
        this.count = repetitions;
    }

    @Override
    public void run() {
        try {
            for(int i = 1; i <= count; i++) {
                account.deposit(amount);
                Thread.sleep(DELAY);
            }
        } catch (InterruptedException e) {
            // No catching!
        }
    }
}
