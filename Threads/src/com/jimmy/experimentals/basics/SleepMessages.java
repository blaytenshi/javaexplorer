package com.jimmy.experimentals.basics;

public class SleepMessages {

    // Sleep times are not guaranteed to be prcise because they are limited by the facilities provided by the underlying OS. Sleep cam be terminated by interrupts!
    // The Interrupted Exception is an exception that will be thrown by sleep when another thread interrupts the current thread while sleep is active.
    public static void main(String args[]) throws InterruptedException {
        String importantInfo[] = {
                "Mares eat oats",
                "Does eat oats",
                "Little lambs eat ivy",
                "A kid will eat ivy too"
        };

        for (int i = 0; i < importantInfo.length; i++) {
            // Pause for 4 seconds
            Thread.sleep(4000);
            // Print a message
            System.out.println(importantInfo[i]);
        }

        // Application will terminate after it has completed execution
    }

}
