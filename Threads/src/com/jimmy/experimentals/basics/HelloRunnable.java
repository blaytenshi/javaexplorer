package com.jimmy.experimentals.basics;

// This method is more general because the Runnable object can subclass a class other than Thread. It seperates the Runnable task from the Thread object that executes the task.
public class HelloRunnable implements Runnable {

    public static void main(String[] args) {
        // instantiates a HelloRunnable Object, passes it to a new (anonymous) Thread object and calls the start() of Thread object
        (new Thread(new HelloRunnable())).start();
    }

    @Override
    public void run() {
        System.out.println("Hello from a thread!");
    }

}
