package com.jimmy.experimentals.basics;

// Easier ot use in simple applications but is limited by the fact that your task class (HelloThread) must be a descendent of Thread.
public class HelloThread extends Thread {

    public void run() {
        System.out.println("Hello from a thread!");
    }

    public static void main(String args[]) {
        (new HelloThread()).start();
    }

}
