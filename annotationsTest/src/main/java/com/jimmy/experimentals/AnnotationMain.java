package com.jimmy.experimentals;

import java.lang.reflect.Method;

/**
 * Created by jimmy.tang on 6/06/2017.
 */
public class AnnotationMain {

    public static void main(String... args) throws ClassNotFoundException, NoSuchMethodException {

        // forName() must be the fully qualified class name (with package names)
        // getMethod() must declare name of method as well as an array of method's parameter types (due to method overloading). If there's only one type, just put that in.
        Method m = Class.forName("com.jimmy.experimentals.Block")
                .getMethod("travelThroughTime", String.class);

        if (m.isAnnotationPresent(RequestForEnhancement.class)) {
            try {
                m.invoke(null, "Blah");
            } catch (Throwable ex) {
                ex.getCause();
            }
        }

    }

}
