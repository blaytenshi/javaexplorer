package com.jimmy.experimentals;

/**
 * Created by jimmy.tang on 6/06/2017.
 */
public class Block {

    public Block() {

    }

    @RequestForEnhancement(
            id = 2868724,
            synopsis = "Enable time travel",
            engineer = "Mr. Peabody",
            date = "4/1/3007"
    )
    public static void travelThroughTime(String date) {
        System.out.println(date);
    }

}
