public class Square extends Shape {

    public Square() {
        super(1); // Shape now has a constructor method... Will print "Shape Boo"
        System.out.println("Square Boo");
    }

    public void callSuperMessage(String s) {
        super.message(s);
    }

}
