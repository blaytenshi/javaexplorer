package com.jimmy.experimentals.ByteStreams;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;

/**
 * I/O Streams - Byte Streams
 */
public class CopyBytes {

    public static void main(String[] args) throws IOException {

        FileInputStream in = null;
        FileOutputStream out = null;

        try {
            // xanadu.txt is in the classpath (root of the project folder)
            in = new FileInputStream("xanadu.txt");
            // application will create it if doesn't already exist (and if OS allows)
            out = new FileOutputStream("outagain.txt");

            int c;

            while ((c = in.read()) != -1) {
                // will print out the actual byte values
                System.out.println(c);
                // writes the byte values into the given file
                out.write(c);
            }

        } finally {
            if (in != null) {
                in.close();
            }
            if (out != null) {
                out.close();
            }
        }

    }
}
