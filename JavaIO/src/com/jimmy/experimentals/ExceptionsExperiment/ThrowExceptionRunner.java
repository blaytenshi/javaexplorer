package com.jimmy.experimentals.ExceptionsExperiment;

import java.io.IOException;

/**
 * Created by jimmy.tang on 19/12/2016.
 */
public class ThrowExceptionRunner {

    ThrowException throwException;

    public ThrowExceptionRunner() {
        this.throwException = new ThrowException();
    }

    public void testThrowExceptions() throws IOException {
        throwException.throwIOException(2);
        throwException.throwBadNumberException(4);
    }

}
