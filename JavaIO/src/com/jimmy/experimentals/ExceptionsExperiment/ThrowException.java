package com.jimmy.experimentals.ExceptionsExperiment
        ;

import com.jimmy.experimentals.ExceptionsExperiment.CustomExceptions.BadNumberException;

import java.io.IOException;

public class ThrowException {

    public ThrowException() {

    }

    public int throwIOException(int value) throws IOException {
        if (value%2 == 0) {
            return 1;
        } else {
            throw new IOException("io exception");
        }
    }

    public int throwBadNumberException(int value) throws BadNumberException {
        if (value%5 == 0) {
            return 1;
        } else {
            throw new BadNumberException("bad number");
        }
    }

}
