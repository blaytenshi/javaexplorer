package com.jimmy.experimentals.ExceptionsExperiment;

import com.jimmy.experimentals.ExceptionsExperiment.CustomExceptions.BadNumberException;

import java.io.IOException;

public class Exceptions {

    public static void main(String[] args) {

        ThrowExceptionRunner throwExceptionRunner = new ThrowExceptionRunner();

        try {
            throwExceptionRunner.testThrowExceptions();
        } catch (BadNumberException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

}