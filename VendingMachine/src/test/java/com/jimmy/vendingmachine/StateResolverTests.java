package com.jimmy.vendingmachine;

import org.junit.Before;
import org.junit.Test;

import static org.assertj.core.api.Assertions.assertThat;

public class StateResolverTests {

    private StateResolver stateResolver;

    @Before
    public void setUp() {
        stateResolver = new StateResolver();
    }

    @Test
    public void shouldReturnCurrentState() {

        assertThat(stateResolver.getMachineState()).isEqualTo(State.INITIAL);

    }

    @Test
    public void shouldReturnNextStateAfterWelcome() {

        stateResolver.moveToNextState();

        assertThat(stateResolver.getMachineState()).isEqualTo(State.WELCOME);

    }

    @Test
    public void shouldReturnSameStateIfStateIsFinalState() {
        stateResolver.moveToNextState(); // Initial -> Welcome
        stateResolver.moveToNextState(); // Welcome -> Finish
        stateResolver.moveToNextState(); // Finish -> Finish (Does not change)
        assertThat(stateResolver.getMachineState()).isEqualTo(State.FINISH);
    }

}
