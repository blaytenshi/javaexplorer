package com.jimmy.vendingmachine;

import org.junit.Before;
import org.junit.Test;

import static org.assertj.core.api.Assertions.assertThat;

public class MessageServiceTests {

    MessageService messageService;

    @Before
    public void setUp() {
        messageService = new MessageServiceImpl();
    }

    @Test
    public void getMessageShouldReturnWelcomeStateSpecificMessage() {

        String returnMessage = messageService.getMessage(State.WELCOME);

        assertThat(returnMessage).isEqualTo("Welcome to the Vending Machine!");

    }

    @Test
    public void getMessageShouldReturnListItemStateSpecificMessage() {

        String returnMessage = messageService.getMessage(State.LIST_ITEMS);

        assertThat(returnMessage).isEqualTo("Here are the available items: ");

    }

}
