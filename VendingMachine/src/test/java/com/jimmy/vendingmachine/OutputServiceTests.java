package com.jimmy.vendingmachine;

import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.springframework.boot.test.rule.OutputCapture;

import java.util.ArrayList;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class OutputServiceTests {

    private String output = "";
    private ArrayList<String> itemList;
    private ArrayList<String> emptyList;
    private OutputService outputService;
    private MessageService mockMessageService;
    private State state;

    @Rule
    public OutputCapture outputCapture = new OutputCapture();

    @Before
    public void setUp() {
        mockMessageService = mock(MessageService.class);
        when(mockMessageService.getMessage(State.WELCOME)).thenReturn("Welcome to the Vending Machine!");
        when(mockMessageService.getMessage(State.LIST_ITEMS)).thenReturn("Here are the available items: ");
        outputService = new OutputServiceImpl(mockMessageService);

        itemList = new ArrayList<>();
        itemList.add("Banana");
        itemList.add("Chips");
        itemList.add("Mountain Dew");

        emptyList = new ArrayList<>();
    }

    @After
    public void tearDown() {
        output = "";

    }

    @Test
    public void outputServicePrintMessageShouldOutputGivenMessage() {
        String message = "Message";

        boolean isPrinted = outputService.printMessage(message);

        output = this.outputCapture.toString();

        assertThat(isPrinted).isTrue();
        assertThat(output).contains(message);

    }

    @Test
    public void outputServiceShouldPrintGivenItems() {
        boolean isPrinted = outputService.printMessage(itemList);

        output = this.outputCapture.toString();

        assertThat(isPrinted).isTrue();
        assertThat(output).contains(itemList);
    }

    @Test
    public void outputServiceShouldPrintWelcomeMessageOnWelcomeState() {
        state = State.WELCOME;

        boolean isPrinted = outputService.printMessage(state);

        output = this.outputCapture.toString();

        assertThat(isPrinted).isTrue();
        assertThat(output).contains("Welcome to the Vending Machine!");
    }

    @Test
    public void outputServiceShouldPrintListOfItemsOnListItemsState() {
        state = State.LIST_ITEMS;

        boolean isPrinted = outputService.printMessage(state);

        output = this.outputCapture.toString();

        assertThat(isPrinted).isTrue();
        assertThat(output).contains("Here are the available items: ");
    }

}
