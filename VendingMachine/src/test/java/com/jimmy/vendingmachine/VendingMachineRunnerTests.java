package com.jimmy.vendingmachine;

import org.junit.Rule;
import org.junit.Test;
import org.springframework.boot.test.rule.OutputCapture;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.*;

public class VendingMachineRunnerTests {

    @Rule
    public OutputCapture outputCapture = new OutputCapture();

    @Test
    public void shouldShowWelcomeMessageWhenInitialising() throws Exception {

        MessageService messageService = mock(MessageService.class);
        OutputService outputService = mock(OutputService.class);
        StateResolver stateResolver = mock(StateResolver.class);
        when(messageService.getMessage(State.WELCOME)).thenReturn("Welcome to the Vending Machine!");
        when(stateResolver.getMachineState()).thenReturn(State.WELCOME);

        VendingMachine vendingMachine = new VendingMachine(outputService, stateResolver);
        vendingMachine.run();

        verify(messageService).getMessage(State.WELCOME);
        verify(stateResolver).getMachineState();
        verify(outputService).printMessage("Welcome to the Vending Machine!");
        assertThat(stateResolver.getMachineState()).isEqualTo(State.WELCOME);

    }

}
