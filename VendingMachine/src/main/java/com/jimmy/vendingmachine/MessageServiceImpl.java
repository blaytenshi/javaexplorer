package com.jimmy.vendingmachine;

public class MessageServiceImpl implements MessageService {

    private static final String INITIAL = "";
    private static final String WELCOME = "Welcome to the Vending Machine!";
    private static final String LIST_ITEMS = "Here are the available items: ";
    private static final String FINISH = "";

    @Override
    public String getMessage(State state) {
        switch (state) {
            case INITIAL:
                return INITIAL;
            case WELCOME:
                return WELCOME;
            case LIST_ITEMS:
                StringBuilder sb
                return LIST_ITEMS;
            case FINISH:
                return FINISH;
            default:
                return "";
        }
    }
}
