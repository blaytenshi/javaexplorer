package com.jimmy.vendingmachine;

public enum State {
    INITIAL {
        @Override
        State moveToNextState() {
            return WELCOME;
        }
    },
    WELCOME {
        @Override
        State moveToNextState() {
            return LIST_ITEMS;
        }
    },
    LIST_ITEMS{
        @Override
        State moveToNextState() {
            return FINISH;
        }
    },
    FINISH {
        @Override
        State moveToNextState() {
            return this;
        }
    };

    abstract State moveToNextState();

}
