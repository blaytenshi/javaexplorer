package com.jimmy.vendingmachine;

public class VendingMachine {

    private final OutputService outputService;
    private final StateResolver stateResolver;

    public VendingMachine(
            OutputService outputService,
            StateResolver stateResolver) {
        this.outputService = outputService;
        this.stateResolver = stateResolver;
    }

    public void run() {

        State state = stateResolver.getMachineState();

        while (!state.equals(State.FINISH)) {
            outputService.printMessage(state);
            stateResolver.moveToNextState();
            state = stateResolver.getMachineState();
        }

    }
}
