package com.jimmy.vendingmachine;

import java.util.List;

public interface OutputService {

    boolean printMessage(String message);

    boolean printMessage(List<String> itemList);

    boolean printMessage(State state);
}
