package com.jimmy.vendingmachine;

import java.util.List;

public class OutputServiceImpl implements OutputService {

    MessageService messageService;

    public OutputServiceImpl(MessageService messageService) {
        this.messageService = messageService;
    }

    @Override
    public boolean printMessage(String message) {
        System.out.println(message);
        return true;
    }

    @Override
    public boolean printMessage(List<String> itemList) {

        for (int i = 0; i < itemList.size(); i++) {
            StringBuffer sb = new StringBuffer();
            sb.append(i+1);
            sb.append(" ");
            sb.append(itemList.get(i));
            System.out.println(sb.toString());
        }

        return true;
    }

    @Override
    public boolean printMessage(State state) {

        System.out.println(messageService.getMessage(state));

        return true;
    }

}
