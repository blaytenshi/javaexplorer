package com.jimmy.vendingmachine;

public interface MessageService {

    String getMessage(State state);

}
