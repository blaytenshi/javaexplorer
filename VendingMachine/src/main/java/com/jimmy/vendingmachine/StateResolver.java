package com.jimmy.vendingmachine;

public class StateResolver {

    private State machineState;

    public StateResolver() {
        this.machineState = State.INITIAL;
    }

    public State getMachineState() {
        return machineState;
    }

    public boolean moveToNextState() {
        machineState = machineState.moveToNextState();
        return true;
    }
}
