package com.jimmy.vendingmachine;

public class VendingMachineRunner {

    public static void main(String ... args) {

        MessageService messageService = new MessageServiceImpl();
        OutputService outputService = new OutputServiceImpl(messageService);
        StateResolver stateResolver = new StateResolver();

        VendingMachine vendingMachine = new VendingMachine(outputService, stateResolver);
        vendingMachine.run();
    }

}
